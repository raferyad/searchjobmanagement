﻿namespace SearchJobManagement.Mvc.Areas.Admin.Infrastructure.Session
{
    public interface ISessionManager
    {
        UserSession User { get; set; }
        void Clear();
    }
}
