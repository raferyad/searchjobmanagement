﻿using SearchJobManagement.Mvc.Models.Client.Entities;
using SearchJobManagement.Mvc.Models.Global.Entities;

namespace SearchJobManagement.Mvc.Areas.Admin.Infrastructure.Session
{
    public class UserSession
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public UserRole Role { get; set; }
        public string Token { get; set; }
    }
}