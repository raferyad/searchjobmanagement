﻿using Microsoft.AspNetCore.Mvc;
using SearchJobManagement.Models.Repositories;
using SearchJobManagement.Mvc.Areas.Admin.Infrastructure.Security;
using SearchJobManagement.Mvc.Areas.Admin.Infrastructure.Session;
using SearchJobManagement.Mvc.Areas.Admin.Models.Contacts;
using SearchJobManagement.Mvc.Models.Client.Entities;

namespace SearchJobManagement.Mvc.Areas.Admin.Controllers
{
    [Area("Admin")]
    [AuthRequired]
    public class ContactController : Controller
    {
        private readonly IContactRepository<ContactBO> _contactRepository;
        private readonly ISessionManager _sessionManager;

        public ContactController(IContactRepository<ContactBO> contactRepository, ISessionManager sessionManager)
        {
            _contactRepository = contactRepository;
            _sessionManager = sessionManager;
        }

        // GET: ContactController
        public IActionResult Index()
        {
            return View(_contactRepository.GetAll().Select(c => new DisplayContact() { Id = c.Id, LastName = c.LastName, FirstName = c.FirstName }));
        }

        public IActionResult Details(int id)
        {
            ContactBO cbo = _contactRepository.GetById(id);
            Contact contact = new Contact() { Id = cbo.Id, LastName = cbo.LastName, FirstName = cbo.FirstName };

            if (contact is null)
                return RedirectToAction("Index");

            return View(new DisplayContact() { Id = contact.Id, LastName = contact.LastName, FirstName = contact.FirstName, Email = contact.Email});
        }

        public IActionResult Create()
        {
            CreateContactForm form = new CreateContactForm();

            return View(form);
        }

        [HttpPost]
        public IActionResult Create(CreateContactForm form)
        {
            if (!ModelState.IsValid)
            {
                return View(form);
            }

            ContactBO newContact = new ContactBO(form.FirstName, form.LastName, form.Email, form.Phone);

            ContactBO cbo = _contactRepository.Insert(newContact);

            return RedirectToAction("Index");
        }

        // GET: ContactController/Edit/5
        public ActionResult Edit(int id)
        {
            ContactBO cbo = _contactRepository.GetById(id);

            if (cbo is null)
                return RedirectToAction("Index");

            EditContactForm form = new EditContactForm()
            {
                Id = cbo.Id,
                LastName = cbo.LastName,
                FirstName = cbo.FirstName,
                Email = cbo.Email,
                Phone = cbo.Phone
            };

            return View(form);
        }

        // POST: ContactController/Edit/5
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit(int id, EditContactForm form)
        {
            if (!ModelState.IsValid)
            {
                return View(form);
            }

            bool isUpdated = _contactRepository.Update(id, new ContactBO(form.FirstName, form.LastName, form.Email, form.Phone));
            
            if(!isUpdated)
                return View(form);

            return RedirectToAction("Index");
        }

        // GET: ContactController/Delete/5
        public ActionResult Delete(int id)
        {
            ContactBO cbo = _contactRepository.GetById(id);

            if (cbo is null)
                return RedirectToAction("Index");

            return View(new DisplayContact() { Id = cbo.Id, LastName = cbo.LastName, FirstName = cbo.FirstName, Email = cbo.Email, Phone = cbo.Phone });
        }

        // POST: ContactController/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, DisplayContact form)
        {
            bool isDeleted = _contactRepository.Delete(id);

            if (!isDeleted)
                return View(form);

            return RedirectToAction("Index");
        }
    }
}
