﻿using Microsoft.AspNetCore.Mvc;
using SearchJobManagement.Mvc.Areas.Admin.Infrastructure.Security;
using SearchJobManagement.Mvc.Models;
using System.Diagnostics;

namespace SearchJobManagement.Mvc.Areas.Admin.Controllers
{
    [Area("Admin")]
    [AuthRequired]
    public class DashboardController : Controller
    {
        private readonly ILogger<DashboardController> _logger;

        public DashboardController(ILogger<DashboardController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}