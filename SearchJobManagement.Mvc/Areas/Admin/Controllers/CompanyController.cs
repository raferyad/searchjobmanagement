﻿using Microsoft.AspNetCore.Mvc;
using SearchJobManagement.Models.Repositories;
using SearchJobManagement.Mvc.Areas.Admin.Infrastructure.Security;
using SearchJobManagement.Mvc.Areas.Admin.Infrastructure.Session;
using SearchJobManagement.Mvc.Areas.Admin.Models.Companies;
using SearchJobManagement.Mvc.Models.Client.Entities;

namespace SearchJobManagement.Mvc.Areas.Admin.Controllers
{
    [Area("Admin")]
    [AuthRequired]
    public class CompanyController : Controller
    {
        private readonly ICompanyRepository<CompanyBO> _CompanyRepository;
        private readonly ISessionManager _sessionManager;

        public CompanyController(ICompanyRepository<CompanyBO> CompanyRepository, ISessionManager sessionManager)
        {
            _CompanyRepository = CompanyRepository;
            _sessionManager = sessionManager;
        }

        // GET: CompanyController
        public IActionResult Index()
        {
            return View(_CompanyRepository.GetAll().Select(c => new DisplayCompany() { Id = c.Id, Name = c.Name, Website = c.Website }));
        }

        public IActionResult Details(int id)
        {
            CompanyBO cbo = _CompanyRepository.GetById(id);
            Company Company = new Company() { Id = cbo.Id, Name = cbo.Name, Website = cbo.Website, CreatorId = cbo.CreatorId };

            if (Company is null)
                return RedirectToAction("Index");

            return View(new DisplayCompany() { Id = Company.Id, Name = cbo.Name, Website = cbo.Website, CreatorId = cbo.CreatorId });
        }

        public IActionResult Create()
        {
            CreateCompanyForm form = new CreateCompanyForm();

            return View(form);
        }

        [HttpPost]
        public IActionResult Create(CreateCompanyForm form)
        {
            if (!ModelState.IsValid)
            {
                return View(form);
            }

            CompanyBO newCompany = new CompanyBO(form.Name, form.Website, _sessionManager.User.Id);

            CompanyBO cbo = _CompanyRepository.Insert(newCompany);

            return RedirectToAction("Index");
        }

        // GET: CompanyController/Edit/5
        public ActionResult Edit(int id)
        {
            CompanyBO cbo = _CompanyRepository.GetById(id);

            if (cbo is null)
                return RedirectToAction("Index");

            EditCompanyForm form = new EditCompanyForm()
            {
                Id = cbo.Id,
                Name = cbo.Name,
                Website = cbo.Website
            };

            return View(form);
        }

        // POST: CompanyController/Edit/5
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit(int id, EditCompanyForm form)
        {
            if (!ModelState.IsValid)
            {
                return View(form);
            }

            bool isUpdated = _CompanyRepository.Update(id, new CompanyBO(form.Name, form.Website, _sessionManager.User.Id));

            if (!isUpdated)
                return View(form);

            return RedirectToAction("Index");
        }

        // GET: CompanyController/Delete/5
        public ActionResult Delete(int id)
        {
            CompanyBO cbo = _CompanyRepository.GetById(id);

            if (cbo is null)
                return RedirectToAction("Index");

            return View(new DisplayCompany() { Id = cbo.Id, Name = cbo.Name, Website = cbo.Website, CreatorId = cbo.CreatorId });
        }

        // POST: CompanyController/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, DisplayCompany form)
        {
            bool isDeleted = _CompanyRepository.Delete(id);

            if (!isDeleted)
                return View(form);

            return RedirectToAction("Index");
        }
    }
}
