﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SearchJobManagement.Mvc.Areas.Admin.Infrastructure.Security;

namespace SearchJobManagement.Mvc.Areas.Admin.Controllers
{
    [Area("Dashboard")]
    [AuthRequired]
    public class CandidacyController : Controller
    {
        // GET: CandidacyController
        public ActionResult Index()
        {
            return View();
        }

        // GET: CandidacyController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CandidacyController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CandidacyController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CandidacyController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CandidacyController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CandidacyController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CandidacyController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
