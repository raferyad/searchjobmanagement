﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SearchJobManagement.Mvc.Areas.Admin.Models.Contacts
{
    public class CreateContactForm
    {
        [Required]
        [StringLength(75)]
        [DisplayName("Nom : ")]
        public string LastName { get; set; }
        [Required]
        [StringLength(75)]
        [DisplayName("Prénom : ")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(384)]
        [EmailAddress]
        [DisplayName("Email : ")]
        public string Email { get; set; }
        [DisplayName("Téléphone : ")]
        public string? Phone { get; set; }

    }
}
