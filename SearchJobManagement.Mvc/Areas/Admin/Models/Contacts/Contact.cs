﻿using System.ComponentModel.DataAnnotations;

namespace SearchJobManagement.Mvc.Areas.Admin.Models.Contacts
{
    public class Contact
    {
        public int Id { get; set; }
        [Display(Name = "Prénom")]
        public string? FirstName { get; set; }
        [Display(Name = "Nom")]
        public string? LastName { get; set; }
        public string? Email { get; set; }
        [Display(Name = "Téléphone")]
        public string? Phone { get; set; }

    }
}
