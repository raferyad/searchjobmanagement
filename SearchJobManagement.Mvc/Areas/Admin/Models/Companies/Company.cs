﻿namespace SearchJobManagement.Mvc.Areas.Admin.Models.Companies
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Website { get; set; }
        public int CreatorId { get; set; }
    }
}
