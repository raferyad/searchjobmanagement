﻿namespace SearchJobManagement.Mvc.Areas.Admin.Models.Companies
{
    public class EditCompanyForm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Website { get; set; }
    }
}
