﻿namespace SearchJobManagement.Mvc.Areas.Admin.Models.Companies
{
    public class CreateCompanyForm
    {
        public string Name { get; set; }
        public string Website { get; set; }
    }
}
