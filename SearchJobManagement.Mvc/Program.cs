using SearchJobManagement.Models.Repositories;
using SearchJobManagement.Mvc.Models.Global.Entities;
using SearchJobManagement.Mvc.Models.Global.Repositories;
using SearchJobManagement.Mvc.Models.Client.Entities;
using SearchJobManagement.Mvc.Models.Client.Mappers;
using SearchJobManagement.Mvc.Models.Client.Services;
using SearchJobManagement.Mvc.Areas.Admin.Infrastructure.Session;
using System.Net.Http.Headers;
using Tools.Mappers;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddHttpContextAccessor();
builder.Services.AddDistributedMemoryCache();
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(60);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});
builder.Services.AddTransient(sp =>
{
    HttpClient client = new HttpClient() { BaseAddress = new Uri("https://localhost:7278/") };
    client.DefaultRequestHeaders.Accept.Clear();
    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

    ISessionManager sessionManager = sp.GetService<ISessionManager>();
    if (sessionManager.User is not null)
        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessionManager.User.Token);

    return client;
});

builder.Services.AddScoped<IAuthRepository<User>, AuthRepository>();
builder.Services.AddScoped<IAddressRepository<Address>, AddressRepository>();
builder.Services.AddScoped<ICandidacyRepository<Candidacy>, CandidacyRepository>();
builder.Services.AddScoped<ICityRepository<City>, CityRepository>();
builder.Services.AddScoped<ICommentRepository<Comment>, CommentRepository>();
builder.Services.AddScoped<ICompanyRepository<Company>, CompanyRepository>();
builder.Services.AddScoped<IContactRepository<Contact>, ContactRepository>();
builder.Services.AddScoped<ICountryRepository<Country>, CountryRepository>();
builder.Services.AddScoped<IOfferRepository<Offer>, OfferRepository>();

builder.Services.AddScoped<IAuthRepository<UserBO>, AuthService>();
builder.Services.AddScoped<IAddressRepository<AddressBO>, AddressService>();
builder.Services.AddScoped<ICandidacyRepository<CandidacyBO>, CandidacyService>();
builder.Services.AddScoped<ICityRepository<CityBO>, CityService>();
builder.Services.AddScoped<ICommentRepository<CommentBO>, CommentService>();
builder.Services.AddScoped<ICompanyRepository<CompanyBO>, CompanyService>();
builder.Services.AddScoped<IContactRepository<ContactBO>, ContactService>();
builder.Services.AddScoped<ICountryRepository<CountryBO>, CountryService>();
builder.Services.AddScoped<IOfferRepository<OfferBO>, OfferService>();

builder.Services.AddScoped<ISessionManager, SessionManager>();

builder.Services.AddScoped<IMappersService, MapperClient>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseSession();

app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        name: "Admin",
        pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}/{id?}");
});

app.Run();

#if DEBUG
//    try
//{
//    File.WriteAllText("browsersync-update.txt", DateTime.Now.ToString());
//} catch
//{

//}
#endif