﻿using Microsoft.AspNetCore.Mvc;
using SearchJobManagement.Forms;
using SearchJobManagement.Models.Repositories;
using SearchJobManagement.Mvc.Areas.Admin.Infrastructure.Security;
using SearchJobManagement.Mvc.Areas.Admin.Infrastructure.Session;
using SearchJobManagement.Mvc.Models.Client.Entities;

namespace SearchJobManagement.Mvc.Controllers
{
    public class AuthController : Controller
    {
        private readonly IAuthRepository<UserBO> _authRepository;
        private readonly ISessionManager _sessionManager;

        public AuthController(IAuthRepository<UserBO> authRepository, ISessionManager sessionManager)
        {
            _authRepository = authRepository;
            _sessionManager = sessionManager;
        }

        public IActionResult Index()
        {
            return RedirectToAction("Login");
        }

        [AnonymousRequired]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AnonymousRequired]
        public IActionResult Login(LoginForm form)
        {
            if (!ModelState.IsValid)
                return View(form);

            UserBO user = _authRepository.Login(form.Email, form.Password);

            if (user is null)
            {
                ModelState.AddModelError("", "Email ou mot de passe invalide!!!");
                return View(form);
            }

            _sessionManager.User = new UserSession() { Id = user.Id, LastName = user.LastName, FirstName = user.FirstName, Role = user.Role, Token = user.Token };

            return RedirectToAction("Index", "Dashboard", new { area = "Admin" });
        }

        [AnonymousRequired]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AnonymousRequired]
        public IActionResult Register(RegisterForm form)
        {
            if (!ModelState.IsValid)
                return View(form);

            bool isRegistered = _authRepository.Register(new UserBO(form.FirstName, form.LastName, form.Login, form.Email, form.Password, form.Phone, form.BirthDate));
            
            if (!isRegistered)
                return View(form);
            
            return RedirectToAction("Login");
        }

        [AuthRequired]
        public IActionResult Logout()
        {
            //en ASP .Net MVC Classic ==> Session.Abandon()
            _sessionManager.Clear();
            
            return RedirectToAction("Login");
        }
        //// GET: AuthController
        //public ActionResult Index()
        //{
        //    return View();
        //}

        //// GET: AuthController/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        //// GET: AuthController/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: AuthController/Create
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: AuthController/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: AuthController/Edit/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: AuthController/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: AuthController/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Delete(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
