﻿namespace SearchJobManagement.Api.Models.Client.Entities
{
    public class CandidacyBO
    {
        public int Id { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string Type { get; set; }
        public int OfferId { get; set; }
        internal int UserId { get; private set; }

        public CandidacyBO(DateTime applicationDate, string type, int offerId, int userId)
        {
            ApplicationDate = applicationDate;
            Type = type;
            OfferId = offerId;
            UserId = userId;
        }

        internal CandidacyBO(int id, DateTime applicationDate, string type, int offerId, int userId)
            : this(applicationDate, type, offerId, userId)
        {
            Id = id;
        }

    }
}
