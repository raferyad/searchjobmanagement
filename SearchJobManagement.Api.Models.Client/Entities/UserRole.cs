﻿namespace SearchJobManagement.Api.Models.Client.Entities
{
    public enum UserRole
    {
        ADMIN, USER_SIMPLE, RECRUITER
    }
}
