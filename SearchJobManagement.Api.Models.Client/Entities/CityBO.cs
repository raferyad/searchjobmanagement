﻿namespace SearchJobManagement.Api.Models.Client.Entities
{
    public class CityBO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CounrtyId { get; set; }

        public CityBO(string name, int counrtyId)
        {
            Name = name;
            CounrtyId = counrtyId;
        }

        internal CityBO(int id, string name, int counrtyId)
            : this(name, counrtyId)
        {
            Id = id;
        }
    }
}
