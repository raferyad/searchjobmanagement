﻿namespace SearchJobManagement.Api.Models.Client.Entities
{
    public class UserBO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public DateTime BirthDate { get; set; }
        public UserRole Role { get; set; }
        public string Token { get; set; }

        public UserBO(string firstName, string lastName, string login, string email, string password, string phone, DateTime birthDate)
        {
            FirstName = firstName;
            LastName = lastName;
            Login = login;
            Email = email;
            Password = password;
            Phone = phone;
            BirthDate = birthDate;
        }

        internal UserBO(int id, string firstName, string lastName, string login, string email, string password, string phone, DateTime birthDate, UserRole role)
            : this(firstName, lastName, login, email, password, phone, birthDate)
        {
            Id = id;
            Role = role;
        }
    }
}