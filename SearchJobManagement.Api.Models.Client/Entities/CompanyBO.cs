﻿namespace SearchJobManagement.Api.Models.Client.Entities
{
    public class CompanyBO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Website { get; set; }
        public int CreatorId { get; set; }

        public CompanyBO(string name, string website, int creatorId)
        {
            Name = name;
            Website = website;
            CreatorId = creatorId;
        }

        internal CompanyBO(int id, string name, string website, int creatorId)
            : this(name, website, creatorId)
        {
            Id = id;
        }
    }
}
