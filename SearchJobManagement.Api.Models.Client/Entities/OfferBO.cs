﻿namespace SearchJobManagement.Api.Models.Client.Entities
{
    public class OfferBO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ContractType { get; set; }
        public DateTime PublicationDate { get; set; }
        public string Link { get; set; }
        public string Type { get; set; }
        public int CityId { get; set; }
        public int ContactId { get; set; }
        public int CompanyId { get; set; }
        internal int UserId { get; private set; }

        public OfferBO(string title, string description, string contractType, DateTime publicationDate, string link, string type, int cityId, int contactId, int companyId, int userId)
        {
            Title = title;
            Description = description;
            ContractType = contractType;
            PublicationDate = publicationDate;
            Link = link;
            Type = type;
            CityId = cityId;
            ContactId = contactId;
            CompanyId = companyId;
            UserId = userId;
        }

        internal OfferBO(int id, string title, string description, string contractType, DateTime publicationDate, string link, string type, int cityId, int contactId, int companyId, int userId)
            : this(title, description, contractType, publicationDate, link, type, cityId, contactId, companyId, userId)
        {
            Id = id;
        }
    }
}
