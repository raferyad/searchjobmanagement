﻿using SearchJobManagement.Api.Models.Client.Entities;
using G = SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

namespace SearchJobManagement.Api.Models.Client.Services
{
    public class AuthService : IAuthRepository<UserBO>
    {
        private readonly IAuthRepository<G.User> _authRepository;
        private readonly IMappersService _mappersService;

        public AuthService(IAuthRepository<G.User> authRepository, IMappersService mappersService)
        {
            _authRepository = authRepository;
            _mappersService = mappersService;
        }

        public bool EmailExists(string email)
        {
            return _authRepository.EmailExists(email);
        }

        public UserBO Login(string email, string password)
        {
            return _mappersService.Map<G.User, UserBO>(_authRepository.Login(email, password));
        }

        public bool Register(UserBO user)
        {
            return _authRepository.Register(_mappersService.Map<UserBO, G.User>(user));
        }
    }
}
