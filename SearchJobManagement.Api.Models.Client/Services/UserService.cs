﻿using SearchJobManagement.Api.Models.Client.Entities;
using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

namespace SearchJobManagement.Api.Models.Client.Services
{
    public class UserService : IUserRepository<UserBO>
    {

        private readonly IUserRepository<User> _userRepository;
        private readonly IMappersService _mappersService;

        public UserService(IUserRepository<User> userRepository, IMappersService mappersService)
        {
            _userRepository = userRepository;
            _mappersService = mappersService;
        }

        public bool Delete(int id)
        {
            return _userRepository.Delete(id);
        }

        public IEnumerable<UserBO> GetAll()
        {
            return _userRepository.GetAll().Select(u => _mappersService.Map<User, UserBO>(u));
        }

        public UserBO GetById(int id)
        {
            return _mappersService.Map<User, UserBO>(_userRepository.GetById(id));
        }

        public UserBO Insert(UserBO user)
        {
            return _mappersService.Map<User, UserBO>(_userRepository.Insert(_mappersService.Map<UserBO, User>(user)));
        }

        public bool Update(int id, UserBO user)
        {
            return _userRepository.Update(id, _mappersService.Map<UserBO, User>(user));
        }
    }
}
