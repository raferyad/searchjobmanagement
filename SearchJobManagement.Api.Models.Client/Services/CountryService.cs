﻿using SearchJobManagement.Api.Models.Client.Entities;
using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

namespace SearchJobManagement.Api.Models.Client.Services
{
    public class CountryService : ICountryRepository<CountryBO>
    {

        private readonly ICountryRepository<Country> _countryRepository;
        private readonly IMappersService _mappersService;

        public CountryService(ICountryRepository<Country> countryRepository, IMappersService mappersService)
        {
            _countryRepository = countryRepository;
            _mappersService = mappersService;
        }

        public bool Delete(int id)
        {
            return _countryRepository.Delete(id);
        }

        public IEnumerable<CountryBO> GetAll()
        {
            return _countryRepository.GetAll().Select(u => _mappersService.Map<Country, CountryBO>(u));
        }

        public CountryBO GetById(int id)
        {
            return _mappersService.Map<Country, CountryBO>(_countryRepository.GetById(id));
        }

        public CountryBO Insert(CountryBO country)
        {
            return _mappersService.Map<Country, CountryBO>(_countryRepository.Insert(_mappersService.Map<CountryBO, Country>(country)));
        }

        public bool Update(int id, CountryBO country)
        {
            return _countryRepository.Update(id, _mappersService.Map<CountryBO, Country>(country));
        }
    }
}
