﻿using SearchJobManagement.Api.Models.Client.Entities;
using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

namespace SearchJobManagement.Api.Models.Client.Services
{
    public class AddressService : IAddressRepository<AddressBO>
    {

        private readonly IAddressRepository<Address> _addressRepository;
        private readonly IMappersService _mappersService;

        public AddressService(IAddressRepository<Address> addressRepository, IMappersService mappersService)
        {
            _addressRepository = addressRepository;
            _mappersService = mappersService;
        }

        public bool Delete(int id)
        {
            return _addressRepository.Delete(id);
        }

        public bool Delete(int addressId, int id)
        {
            return _addressRepository.Delete(addressId, id);
        }

        public IEnumerable<AddressBO> GetAll()
        {
            return _addressRepository.GetAll().Select(u => _mappersService.Map<Address, AddressBO>(u));
        }

        public IEnumerable<AddressBO> GetByCity(int cityId)
        {
            return _addressRepository.GetByCity(cityId).Select(u => _mappersService.Map<Address, AddressBO>(u));
        }

        public IEnumerable<AddressBO> GetByCompany(int companyId)
        {
            return _addressRepository.GetByCompany(companyId).Select(u => _mappersService.Map<Address, AddressBO>(u));
        }

        public AddressBO GetById(int id)
        {
            return _mappersService.Map<Address, AddressBO>(_addressRepository.GetById(id));
        }

        public AddressBO Insert(AddressBO address)
        {
            return _mappersService.Map<Address, AddressBO>(_addressRepository.Insert(_mappersService.Map<AddressBO, Address>(address)));
        }

        public bool Update(int id, AddressBO address)
        {
            return _addressRepository.Update(id, _mappersService.Map<AddressBO, Address>(address));
        }
    }
}
