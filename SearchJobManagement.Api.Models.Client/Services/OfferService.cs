﻿using SearchJobManagement.Api.Models.Client.Entities;
using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

namespace SearchJobManagement.Api.Models.Client.Services
{
    public class OfferService : IOfferRepository<OfferBO>
    {

        private readonly IOfferRepository<Offer> _offerRepository;
        private readonly IMappersService _mappersService;

        public OfferService(IOfferRepository<Offer> offerRepository, IMappersService mappersService)
        {
            _offerRepository = offerRepository;
            _mappersService = mappersService;
        }
        public bool ChangeType(int userId, int id, string type)
        {
            return _offerRepository.ChangeType(userId, id, type);
        }

        public bool Delete(int id)
        {
            return _offerRepository.Delete(id);
        }

        public bool Delete(int offerId, int id)
        {
            return _offerRepository.Delete(offerId, id);
        }

        public IEnumerable<OfferBO> GetAll()
        {
            return _offerRepository.GetAll().Select(u => _mappersService.Map<Offer, OfferBO>(u));
        }

        public IEnumerable<OfferBO> GetByCity(int cityId)
        {
            return _offerRepository.GetByCity(cityId).Select(u => _mappersService.Map<Offer, OfferBO>(u));
        }

        public IEnumerable<OfferBO> GetByCompany(int companyId)
        {
            return _offerRepository.GetByCompany(companyId).Select(u => _mappersService.Map<Offer, OfferBO>(u));
        }

        public IEnumerable<OfferBO> GetByContact(int contactId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<OfferBO> GetByUser(int userId)
        {
            return _offerRepository.GetByUser(userId).Select(u => _mappersService.Map<Offer, OfferBO>(u));
        }

        public OfferBO GetById(int id)
        {
            return _mappersService.Map<Offer, OfferBO>(_offerRepository.GetById(id));
        }

        public IEnumerable<OfferBO> GetByType(string type)
        {
            return _offerRepository.GetByType(type).Select(u => _mappersService.Map<Offer, OfferBO>(u));
        }

        public IEnumerable<OfferBO> GetByTitle(string title)
        {
            return _offerRepository.GetByTitle(title).Select(u => _mappersService.Map<Offer, OfferBO>(u));
        }

        public IEnumerable<OfferBO> GetByTitleAndCity(string title, string city)
        {
            return _offerRepository.GetByTitleAndCity(title, city).Select(u => _mappersService.Map<Offer, OfferBO>(u));
        }

        public IEnumerable<OfferBO> GetByUserAndCity(int userId, int cityId)
        {
            return _offerRepository.GetByUserAndCity(userId, cityId).Select(u => _mappersService.Map<Offer, OfferBO>(u));
        }

        public IEnumerable<OfferBO> GetByUserAndCompany(int userId, int companyId)
        {
            return _offerRepository.GetByUserAndCompany(userId, companyId).Select(u => _mappersService.Map<Offer, OfferBO>(u));
        }

        public IEnumerable<OfferBO> GetByUserAndContact(int userId, int contactId)
        {
            return _offerRepository.GetByUserAndContact(userId, contactId).Select(u => _mappersService.Map<Offer, OfferBO>(u));
        }

        public IEnumerable<OfferBO> GetByUserAndType(int userId, string type)
        {
            return _offerRepository.GetByUserAndType(userId, type).Select(u => _mappersService.Map<Offer, OfferBO>(u));
        }

        public OfferBO Insert(OfferBO offer)
        {
            return _mappersService.Map<Offer, OfferBO>(_offerRepository.Insert(_mappersService.Map<OfferBO, Offer>(offer)));
        }

        public bool Update(int id, OfferBO offer)
        {
            return _offerRepository.Update(id, _mappersService.Map<OfferBO, Offer>(offer));
        }
    }
}
