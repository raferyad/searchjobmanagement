﻿using SearchJobManagement.Api.Models.Client.Entities;
using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

namespace SearchJobManagement.Api.Models.Client.Services
{
    public class CityService : ICityRepository<CityBO>
    {

        private readonly ICityRepository<City> _cityRepository;
        private readonly IMappersService _mappersService;

        public CityService(ICityRepository<City> cityRepository, IMappersService mappersService)
        {
            _cityRepository = cityRepository;
            _mappersService = mappersService;
        }

        public bool Delete(int id)
        {
            return _cityRepository.Delete(id);
        }

        public IEnumerable<CityBO> GetAll()
        {
            return _cityRepository.GetAll().Select(u => _mappersService.Map<City, CityBO>(u));
        }

        public IEnumerable<CityBO> GetByCountry(int countryId)
        {
            return _cityRepository.GetByCountry(countryId).Select(c => _mappersService.Map<City, CityBO>(c));
        }

        public CityBO GetById(int id)
        {
            return _mappersService.Map<City, CityBO>(_cityRepository.GetById(id));
        }

        public CityBO Insert(CityBO city)
        {
            return _mappersService.Map<City, CityBO>(_cityRepository.Insert(_mappersService.Map<CityBO, City>(city)));
        }

        public bool Update(int id, CityBO city)
        {
            return _cityRepository.Update(id, _mappersService.Map<CityBO, City>(city));
        }
    }
}
