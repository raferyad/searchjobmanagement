﻿using SearchJobManagement.Api.Models.Client.Entities;
using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

namespace SearchJobManagement.Api.Models.Client.Services
{
    public class CandidacyService : ICandidacyRepository<CandidacyBO>
    {

        private readonly ICandidacyRepository<Candidacy> _candidacyRepository;
        private readonly IMappersService _mappersService;

        public CandidacyService(ICandidacyRepository<Candidacy> candidacyRepository, IMappersService mappersService)
        {
            _candidacyRepository = candidacyRepository;
            _mappersService = mappersService;
        }

        public bool ChangeStatus(int userId, int id, string status)
        {
            return _candidacyRepository.ChangeStatus(userId, id, status);
        }

        public bool Delete(int id)
        {
            return _candidacyRepository.Delete(id);
        }

        public bool Delete(int candidacyId, int id)
        {
            return _candidacyRepository.Delete(candidacyId, id);
        }

        public IEnumerable<CandidacyBO> GetAll()
        {
            return _candidacyRepository.GetAll().Select(u => _mappersService.Map<Candidacy, CandidacyBO>(u));
        }

        public IEnumerable<CandidacyBO> GetByCity(int cityId)
        {
            return _candidacyRepository.GetByCity(cityId).Select(u => _mappersService.Map<Candidacy, CandidacyBO>(u));
        }

        public IEnumerable<CandidacyBO> GetByCompany(int companyId)
        {
            return _candidacyRepository.GetByCompany(companyId).Select(u => _mappersService.Map<Candidacy, CandidacyBO>(u));
        }

        public IEnumerable<CandidacyBO> GetByCreator(int creatorId)
        {
            return _candidacyRepository.GetByCreator(creatorId).Select(u => _mappersService.Map<Candidacy, CandidacyBO>(u));
        }

        public CandidacyBO GetById(int id)
        {
            return _mappersService.Map<Candidacy, CandidacyBO>(_candidacyRepository.GetById(id));
        }

        public IEnumerable<CandidacyBO> GetByOffer(int offerId)
        {
            return _candidacyRepository.GetByOffer(offerId).Select(u => _mappersService.Map<Candidacy, CandidacyBO>(u));
        }

        public IEnumerable<CandidacyBO> GetByStatus(string status)
        {
            return _candidacyRepository.GetByStatus(status).Select(u => _mappersService.Map<Candidacy, CandidacyBO>(u));
        }

        public IEnumerable<CandidacyBO> GetByType(string type)
        {
            return _candidacyRepository.GetByType(type).Select(u => _mappersService.Map<Candidacy, CandidacyBO>(u));
        }

        public IEnumerable<CandidacyBO> GetByUserAndCity(int userId, int cityId)
        {
            return _candidacyRepository.GetByUserAndCity(userId, cityId).Select(u => _mappersService.Map<Candidacy, CandidacyBO>(u));
        }

        public IEnumerable<CandidacyBO> GetByUserAndCompany(int userId, int companyId)
        {
            return _candidacyRepository.GetByUserAndCompany(userId, companyId).Select(u => _mappersService.Map<Candidacy, CandidacyBO>(u));
        }

        public IEnumerable<CandidacyBO> GetByUserAndOffer(int userId, int offerId)
        {
            return _candidacyRepository.GetByUserAndOffer(userId, offerId).Select(u => _mappersService.Map<Candidacy, CandidacyBO>(u));
        }

        public IEnumerable<CandidacyBO> GetByUserAndStatus(int userId, string status)
        {
            return _candidacyRepository.GetByUserAndStatus(userId, status).Select(u => _mappersService.Map<Candidacy, CandidacyBO>(u));
        }

        public CandidacyBO Insert(CandidacyBO candidacy)
        {
            return _mappersService.Map<Candidacy, CandidacyBO>(_candidacyRepository.Insert(_mappersService.Map<CandidacyBO, Candidacy>(candidacy)));
        }

        public bool Update(int id, CandidacyBO candidacy)
        {
            return _candidacyRepository.Update(id, _mappersService.Map<CandidacyBO, Candidacy>(candidacy));
        }
    }
}
