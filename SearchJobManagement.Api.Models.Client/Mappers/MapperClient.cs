﻿using SearchJobManagement.Api.Models.Client.Entities;
using SearchJobManagement.Api.Models.Global.Entities;
using Tools.Mappers;

namespace SearchJobManagement.Api.Models.Client.Mappers
{
    public class MapperClient : MappersService
    {
        protected override void ConfigureMappers(IMappersService service)
        {
            service.Register<UserBO, User>(u => new User()
            {
                Id = u.Id,
                FirstName = u.FirstName,
                LastName = u.LastName,
                Phone = u.Phone,
                Email = u.Email,
                Password = u.Password,
                Login = u.Login,
                BirthDate = u.BirthDate,
                Role = u.Role == UserRole.ADMIN ? "ADMIN" : "USER_SIMPLE"
            });

            service.Register<User, UserBO>(u => new UserBO(
                u.Id, 
                u.FirstName, 
                u.LastName, 
                u.Login, 
                u.Email, 
                u.Password, 
                u.Phone, 
                u.BirthDate,
                u.Role == "ADMIN" ? UserRole.ADMIN : UserRole.USER_SIMPLE
                ));

            service.Register<CompanyBO, Company>(c => new Company()
            {
                Id = c.Id,
                Name = c.Name,
                Website = c.Website,
                CreatorId = c.CreatorId
            });

            service.Register<Company, CompanyBO>(c => new CompanyBO(c.Id, c.Name, c.Website, c.CreatorId));
        }
    }
}
