﻿namespace SearchJobManagement.Api.Models.Global.Entities
{
    public class Address
    {
        public int Id { get; set; }
        public string Street { get; set; }
        public int Number { get; set; }
        public int ZipCode { get; set; }
        public int CityId { get; set; }
        public int CompanyId { get; set; }
    }
}
