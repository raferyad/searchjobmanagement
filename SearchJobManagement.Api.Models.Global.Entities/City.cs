﻿namespace SearchJobManagement.Api.Models.Global.Entities
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CounrtyId { get; set; }
    }
}
