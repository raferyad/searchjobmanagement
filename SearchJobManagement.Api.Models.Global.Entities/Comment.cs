﻿namespace SearchJobManagement.Api.Models.Global.Entities
{
    public class Comment
    {
		public int Id { get; set; }
		public string Content { get; set; }
		public DateTime CreationDate { get; set; }
		public int Star { get; set; }
		public int CreatorId { get; set; }
		public int CompanyId { get; set; }
		public int ParentId { get; set; }
	}
}
