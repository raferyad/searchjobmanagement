﻿namespace SearchJobManagement.Api.Models.Global.Entities
{
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
