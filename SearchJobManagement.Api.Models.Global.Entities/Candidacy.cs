﻿namespace SearchJobManagement.Api.Models.Global.Entities
{
    public class Candidacy
    {
        public int Id { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string Type { get; set; }
        public int OfferId { get; set; }
        public int UserId { get; set; }
    }
}
