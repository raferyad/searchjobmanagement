﻿using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Api.Models.Global.Services.Mappers;
using SearchJobManagement.Models.Repositories;
using Tools.Connections;

namespace SearchJobManagement.Api.Models.Global.Repositories
{
    public class AuthRepository : IAuthRepository<User>
    {
        private readonly Connection _connection;

        public AuthRepository(Connection connection)
        {
            _connection = connection;
        }

        public bool EmailExists(string email)
        {
            Command command = new Command("SP_EmailExists");
            command.AddParameter("Email", email);

            int count = (int) _connection.ExecuteScalar(command);
            return count > 0;
        }

        public User Login(string email, string password)
        {
            Command command = new Command("SP_AuthUser");
            command.AddParameter("Email", email);
            command.AddParameter("Password", password);
            User user = _connection.ExecuteReader(command, dr => dr.MapTo<User>()).SingleOrDefault();
            return user;
        }

        public bool Register(User user)
        {
            Command command = new Command("SP_RegisterUser");
            command.AddParameter("LastName", user.LastName);
            command.AddParameter("FirstName", user.FirstName);
            command.AddParameter("Phone", user.Phone);
            command.AddParameter("BirthDate", user.BirthDate);
            command.AddParameter("Role", "USER_SIMPLE");
            command.AddParameter("Email", user.Email);
            command.AddParameter("Login", user.Login);
            command.AddParameter("Password", user.Password);

            return _connection.ExecuteNonQuery(command) == 1;
        }
    }
}
