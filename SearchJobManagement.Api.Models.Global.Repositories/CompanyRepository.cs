﻿using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Api.Models.Global.Services.Mappers;
using SearchJobManagement.Models.Repositories;
using Tools.Connections;

namespace SearchJobManagement.Api.Models.Global.Repositories
{
    public class CompanyRepository : ICompanyRepository<Company>
    {

        private readonly Connection _connection;

        public CompanyRepository(Connection connection)
        {
            _connection = connection;
        }

        public bool Delete(int id)
        {
            Command command = new Command("SP_DeleteCompany");
            command.AddParameter("Id", id);
            return _connection.ExecuteNonQuery(command) == 1;
        }

        public bool Delete(int userId, int id)
        {
            Command command = new Command("SP_DeleteCompany");
            command.AddParameter("UserId", userId);
            command.AddParameter("Id", id);

            return _connection.ExecuteNonQuery(command) == 1;
        }

        public IEnumerable<Company> GetAll()
        {
            Command command = new Command("SP_GetAllCompanies");
            return _connection.ExecuteReader(command, dr => dr.MapTo<Company>());
        }

        public IEnumerable<Company> GetByCreator(int creatorId)
        {
            Command command = new Command("SP_GetByUser");
            command.AddParameter("Table", "Companies");
            command.AddParameter("CreatorId", creatorId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Company>());
        }

        public Company GetById(int id)
        {
            Command command = new Command("SP_GetCompanyById");
            command.AddParameter("Id", id);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Company>()).SingleOrDefault();
        }

        public IEnumerable<Company> GetByName(string name)
        {
            Command command = new Command("SP_GetCompaniesByName");
            command.AddParameter("Name", name);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Company>());
        }

        public Company Insert(Company company)
        {
            Command command = new Command("SP_AddCompany");
            command.AddParameter("Name", company.Name);
            command.AddParameter("Website", company.Website);
            command.AddParameter("CreatorId", company.CreatorId);

            company.Id = (int)_connection.ExecuteScalar(command);

            return company;
        }

        public bool Update(int id, Company company)
        {
            Command command = new Command("SP_UpdateCompany");
            command.AddParameter("Id", id);
            command.AddParameter("Name", company.Name);
            command.AddParameter("Website", company.Website);

            return _connection.ExecuteNonQuery(command) == 1;
        }

        public bool Update(int userId, int id, Company company)
        {
            if(userId != company.CreatorId)
                return false;

            Command command = new Command("SP_UpdateCompany");
            command.AddParameter("Id", id);
            command.AddParameter("Name", company.Name);
            command.AddParameter("Website", company.Website);

            return _connection.ExecuteNonQuery(command) == 1;
        }
    }
}
