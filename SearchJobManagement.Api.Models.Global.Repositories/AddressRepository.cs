﻿using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Api.Models.Global.Services.Mappers;
using SearchJobManagement.Models.Repositories;
using Tools.Connections;

namespace SearchJobManagement.Api.Models.Global.Repositories
{
    public class AddressRepository : IUserRepository<Address>
    {

        private readonly Connection _connection;

        public AddressRepository(Connection connection)
        {
            _connection = connection;
        }

        public bool Delete(int id)
        {
            Command command = new Command("SP_DeleteAddress");
            command.AddParameter("Id", id);
            return _connection.ExecuteNonQuery(command) == 1;
        }

        public IEnumerable<Address> GetAll()
        {
            Command command = new Command("SP_GetAllAddresses");
            return _connection.ExecuteReader(command, dr => dr.MapTo<Address>());
        }

        public Address GetById(int id)
        {
            Command command = new Command("SP_GetAddressById");
            command.AddParameter("Id", id);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Address>()).SingleOrDefault();
        }

        public Address Insert(Address address)
        {
            Command command = new Command("SP_AddAddress");
            command.AddParameter("Street", address.Street);
            command.AddParameter("Number", address.Number);
            command.AddParameter("ZipCode", address.ZipCode);
            command.AddParameter("CityId", address.CityId);
            command.AddParameter("CompanyId", address.CompanyId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Address>()).SingleOrDefault();
        }

        public bool Update(int id, Address address)
        {
            Command command = new Command("SP_UpdateAddress");
            command.AddParameter("Id", id);
            command.AddParameter("Street", address.Street);
            command.AddParameter("Number", address.Number);
            command.AddParameter("ZipCode", address.ZipCode);
            command.AddParameter("CityId", address.CityId);

            return _connection.ExecuteNonQuery(command) == 1;
        }
    }
}
