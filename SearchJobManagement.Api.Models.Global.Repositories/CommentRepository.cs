﻿using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Api.Models.Global.Services.Mappers;
using SearchJobManagement.Models.Repositories;
using Tools.Connections;

namespace SearchJobManagement.Api.Models.Global.Repositories
{
    public class CommentRepository : ICommentRepository<Comment>
    {

        private readonly Connection _connection;

        public CommentRepository(Connection connection)
        {
            _connection = connection;
        }

        public bool Delete(int id)
        {
            Command command = new Command("SP_DeleteComment");
            command.AddParameter("Id", id);
            return _connection.ExecuteNonQuery(command) == 1;
        }

        public bool Delete(int userId, int id)
        {
            Command command = new Command("SP_DeleteComment");
            command.AddParameter("CreatorId", userId);
            command.AddParameter("Id", id);

            return _connection.ExecuteNonQuery(command) == 1;
        }

        public IEnumerable<Comment> GetAll()
        {
            Command command = new Command("SP_GetAllComments");
            return _connection.ExecuteReader(command, dr => dr.MapTo<Comment>());
        }

        public IEnumerable<Comment> GetByCompany(int companyId)
        {
            Command command = new Command("SP_GetByCompany");
            command.AddParameter("Table", "Comments");
            command.AddParameter("CompanyId", companyId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Comment>());
        }

        public IEnumerable<Comment> GetByCreator(int creatorId)
        {
            Command command = new Command("SP_GetByUser");
            command.AddParameter("Table", "Comments");
            command.AddParameter("CreatorId", creatorId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Comment>());
        }

        public Comment GetById(int id)
        {
            Command command = new Command("SP_GetCommentById");
            command.AddParameter("Id", id);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Comment>()).SingleOrDefault();
        }

        public IEnumerable<Comment> GetByOffer(int offerId)
        {
            Command command = new Command("SP_GetByOffer");
            command.AddParameter("Table", "Comments");
            command.AddParameter("OfferId", offerId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Comment>());
        }

        public Comment Insert(Comment comment)
        {
            Command command = new Command("SP_AddComment");
            command.AddParameter("Content", comment.Content);
            command.AddParameter("CreationDate", comment.CreationDate);
            command.AddParameter("Star", comment.Star);
            command.AddParameter("CompanyId", comment.CompanyId);
            command.AddParameter("ParentId", comment.ParentId);
            command.AddParameter("CreatorId", comment.CreatorId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Comment>()).SingleOrDefault();
        }

        public bool Update(int id, Comment comment)
        {
            if (comment.Id != id)
                return false;

            Command command = new Command("SP_UpdateComment");
            command.AddParameter("Id", id);
            command.AddParameter("Content", comment.Content);
            command.AddParameter("Star", comment.Star);

            return _connection.ExecuteNonQuery(command) == 1;
        }

        public bool Update(int userId, int id, Comment comment)
        {
            if(userId == comment.CreatorId)
                return Update(id, comment);

            return false;
        }
    }
}
