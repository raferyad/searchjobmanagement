﻿using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Api.Models.Global.Services.Mappers;
using SearchJobManagement.Models.Repositories;
using Tools.Connections;

namespace SearchJobManagement.Api.Models.Global.Repositories
{
    public class CandidacyRepository : ICandidacyRepository<Candidacy>
    {

        private readonly Connection _connection;

        public CandidacyRepository(Connection connection)
        {
            _connection = connection;
        }

        public bool ChangeStatus(int userId, int id, string status)
        {
            Command command = new Command("SP_ChangeCandidacyStatus");
            command.AddParameter("UserId", userId);
            command.AddParameter("Id", id);
            command.AddParameter("Status", status);

            return _connection.ExecuteNonQuery(command) == 1;
        }

        public bool Delete(int id)
        {
            Command command = new Command("SP_DeleteCandidacy");
            command.AddParameter("Id", id);
            return _connection.ExecuteNonQuery(command) == 1;
        }

        public bool Delete(int userId, int id)
        {
            Command command = new Command("SP_DeleteCandidacy");
            command.AddParameter("UserId", userId);
            command.AddParameter("Id", id);

            return _connection.ExecuteNonQuery(command) == 1;
        }

        public IEnumerable<Candidacy> GetAll()
        {
            Command command = new Command("SP_GetAllCandidacies");
            return _connection.ExecuteReader(command, dr => dr.MapTo<Candidacy>());
        }

        public IEnumerable<Candidacy> GetByCity(int cityId)
        {
            Command command = new Command("SP_GetByCity");
            command.AddParameter("Table", "Candidacies");
            command.AddParameter("CityId", cityId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Candidacy>());
        }

        public IEnumerable<Candidacy> GetByCompany(int companyId)
        {
            Command command = new Command("SP_GetByCompany");
            command.AddParameter("Table", "Candidacies");
            command.AddParameter("CompanyId", companyId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Candidacy>());
        }

        public IEnumerable<Candidacy> GetByContact(int contactId)
        {
            Command command = new Command("SP_GetByContact");
            command.AddParameter("Table", "Candidacies");
            command.AddParameter("ContactId", contactId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Candidacy>());
        }

        public Candidacy GetById(int id)
        {
            Command command = new Command("SP_GetCandidacyById");
            command.AddParameter("Id", id);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Candidacy>()).SingleOrDefault();
        }

        public IEnumerable<Candidacy> GetByOffer(int offerId)
        {
            Command command = new Command("SP_GetByOffer");
            command.AddParameter("Table", "Candidacies");
            command.AddParameter("OfferId", offerId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Candidacy>());
        }

        public IEnumerable<Candidacy> GetByStatus(string status)
        {
            Command command = new Command("SP_GetCandidaciesByStatus");
            command.AddParameter("Status", status);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Candidacy>());
        }

        public IEnumerable<Candidacy> GetByType(string type)
        {
            Command command = new Command("SP_GetCandidaciesByType");
            command.AddParameter("Type", type);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Candidacy>());
        }

        public IEnumerable<Candidacy> GetByCreator(int creatorId)
        {
            Command command = new Command("SP_GetByUser");
            command.AddParameter("Table", "Candidacies");
            command.AddParameter("UserId", creatorId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Candidacy>());
        }

        public IEnumerable<Candidacy> GetByUserAndCity(int userId, int cityId)
        {
            Command command = new Command("SP_GetCandidaciesByUserAndCity");
            command.AddParameter("CreatorId", userId);
            command.AddParameter("CityId", cityId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Candidacy>());
        }

        public IEnumerable<Candidacy> GetByUserAndCompany(int userId, int companyId)
        {
            Command command = new Command("SP_GetCandidaciesByUserAndCompany");
            command.AddParameter("CreatorId", userId);
            command.AddParameter("CompanyId", companyId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Candidacy>());
        }

        public IEnumerable<Candidacy> GetByUserAndOffer(int userId, int offerId)
        {
            Command command = new Command("SP_GetCandidaciesByUserAndOffer");
            command.AddParameter("CreatorId", userId);
            command.AddParameter("OfferId", offerId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Candidacy>());
        }

        public IEnumerable<Candidacy> GetByUserAndStatus(int userId, string status)
        {
            Command command = new Command("SP_GetCandidaciesByUserAndStatus");
            command.AddParameter("CreatorId", userId);
            command.AddParameter("Status", status);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Candidacy>());
        }

        public IEnumerable<Candidacy> GetByUserAndType(int userId, string type)
        {
            Command command = new Command("SP_GetCandidacysByUserAndType");
            command.AddParameter("CreatorId", userId);
            command.AddParameter("Type", type);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Candidacy>());
        }

        public Candidacy Insert(Candidacy offer)
        {
            Command command = new Command("SP_AddCandidacy");
            command.AddParameter("Title", offer.ApplicationDate);
            command.AddParameter("Description", offer.Type);
            command.AddParameter("UserId", offer.UserId);
            command.AddParameter("ContactId", offer.OfferId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Candidacy>()).SingleOrDefault();
        }

        public bool Update(int id, Candidacy offer)
        {
            Command command = new Command("SP_UpdateCandidacy");
            command.AddParameter("Id", id);
            command.AddParameter("Title", offer.ApplicationDate);
            command.AddParameter("Description", offer.Type);

            return _connection.ExecuteNonQuery(command) == 1;
        }
    }
}
