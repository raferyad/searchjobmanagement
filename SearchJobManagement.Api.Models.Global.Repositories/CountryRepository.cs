﻿using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Api.Models.Global.Services.Mappers;
using SearchJobManagement.Models.Repositories;
using Tools.Connections;

namespace SearchJobManagement.Api.Models.Global.Repositories
{
    public class CountryRepository : IUserRepository<Country>
    {

        private readonly Connection _connection;

        public CountryRepository(Connection connection)
        {
            _connection = connection;
        }

        public bool Delete(int id)
        {
            Command command = new Command("SP_DeleteCountry");
            command.AddParameter("Id", id);
            return _connection.ExecuteNonQuery(command) == 1;
        }

        public IEnumerable<Country> GetAll()
        {
            Command command = new Command("SP_GetAllCountries");
            return _connection.ExecuteReader(command, dr => dr.MapTo<Country>());
        }

        public Country GetById(int id)
        {
            Command command = new Command("SP_GetCountryById");
            command.AddParameter("Id", id);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Country>()).SingleOrDefault();
        }

        public Country Insert(Country country)
        {
            Command command = new Command("SP_AddCountry");
            command.AddParameter("Name", country.Name);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Country>()).SingleOrDefault();
        }

        public bool Update(int id, Country country)
        {
            Command command = new Command("SP_UpdateCountry");
            command.AddParameter("Id", id);
            command.AddParameter("Name", country.Name);

            return _connection.ExecuteNonQuery(command) == 1;
        }
    }
}
