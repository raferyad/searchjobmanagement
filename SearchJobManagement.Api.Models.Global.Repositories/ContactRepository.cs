﻿using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Api.Models.Global.Services.Mappers;
using SearchJobManagement.Models.Repositories;
using Tools.Connections;

namespace SearchJobManagement.Api.Models.Global.Repositories
{
    public class ContactRepository : IContactRepository<Contact>
    {

        private readonly Connection _connection;

        public ContactRepository(Connection connection)
        {
            _connection = connection;
        }

        public bool Delete(int id)
        {
            Command command = new Command("SP_DeleteContact");
            command.AddParameter("Id", id);
            return _connection.ExecuteNonQuery(command) == 1;
        }

        public bool Delete(int userId, int id)
        {
            Command command = new Command("SP_DeleteContact");
            command.AddParameter("UserId", userId);
            command.AddParameter("Id", id);

            return _connection.ExecuteNonQuery(command) == 1;
        }

        public IEnumerable<Contact> GetAll()
        {
            Command command = new Command("SP_GetAllContacts");
            return _connection.ExecuteReader(command, dr => dr.MapTo<Contact>());
        }

        public Contact GetById(int id)
        {
            Command command = new Command("SP_GetContactById");
            command.AddParameter("Id", id);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Contact>()).SingleOrDefault();
        }

        public Contact GetByOffer(int offerId)
        {
            Command command = new Command("SP_GetContactByOffer");
            command.AddParameter("Table", "Offers");
            command.AddParameter("OfferId", offerId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Contact>()).SingleOrDefault();
        }

        public Contact Insert(Contact contact)
        {
            Command command = new Command("SP_AddContact");
            command.AddParameter("LastName", contact.LastName);
            command.AddParameter("FirstName", contact.FirstName);
            command.AddParameter("Phone", contact.Phone);
            command.AddParameter("Email", contact.Email);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Contact>()).SingleOrDefault();
        }

        public bool Update(int id, Contact contact)
        {
            Command command = new Command("SP_UpdateContact");
            command.AddParameter("Id", id);
            command.AddParameter("LastName", contact.LastName);
            command.AddParameter("FirstName", contact.FirstName);
            command.AddParameter("Phone", contact.Phone);
            command.AddParameter("Email", contact.Email);

            return _connection.ExecuteNonQuery(command) == 1;
        }
    }
}
