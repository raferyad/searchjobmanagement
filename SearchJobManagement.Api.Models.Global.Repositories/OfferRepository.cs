﻿using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Api.Models.Global.Services.Mappers;
using SearchJobManagement.Models.Repositories;
using Tools.Connections;

namespace SearchJobManagement.Api.Models.Global.Repositories
{
    public class OfferRepository : IOfferRepository<Offer>
    {

        private readonly Connection _connection;

        public OfferRepository(Connection connection)
        {
            _connection = connection;
        }

        public bool ChangeType(int userId, int id, string type)
        {
            Command command = new Command("SP_ChangeOfferType");
            command.AddParameter("Id", id);
            command.AddParameter("UserId", userId);
            command.AddParameter("Type", type);

            return _connection.ExecuteNonQuery(command) == 1;
        }

        public bool Delete(int id)
        {
            Command command = new Command("SP_DeleteOffer");
            command.AddParameter("Id", id);
            return _connection.ExecuteNonQuery(command) == 1;
        }

        public bool Delete(int userId, int id)
        {
            Command command = new Command("SP_DeleteOffer");
            command.AddParameter("UserId", userId);
            command.AddParameter("Id", id);

            return _connection.ExecuteNonQuery(command) == 1;
        }

        public IEnumerable<Offer> GetAll()
        {
            Command command = new Command("SP_GetAllOffers");
            return _connection.ExecuteReader(command, dr => dr.MapTo<Offer>());
        }

        public IEnumerable<Offer> GetByCity(int cityId)
        {
            Command command = new Command("SP_GetByCity");
            command.AddParameter("Table", "Offers");
            command.AddParameter("CityId", cityId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Offer>());
        }

        public IEnumerable<Offer> GetByCompany(int companyId)
        {
            Command command = new Command("SP_GetByCompany");
            command.AddParameter("Table", "Offers");
            command.AddParameter("CompanyId", companyId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Offer>());
        }

        public IEnumerable<Offer> GetByContact(int contactId)
        {
            Command command = new Command("SP_GetByContact");
            command.AddParameter("Table", "Offers");
            command.AddParameter("ContactId", contactId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Offer>());
        }

        public Offer GetById(int id)
        {
            Command command = new Command("SP_GetOfferById");
            command.AddParameter("Id", id);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Offer>()).SingleOrDefault();
        }

        public IEnumerable<Offer> GetByTitle(string title)
        {
            Command command = new Command("SP_GetOffersByTitle");
            command.AddParameter("Title", title);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Offer>());
        }

        public IEnumerable<Offer> GetByTitleAndCity(string title, string city)
        {
            Command command = new Command("SP_GetOffersByTitleAndCity");
            command.AddParameter("Title", title);
            command.AddParameter("City", city);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Offer>());
        }

        public IEnumerable<Offer> GetByType(string type)
        {
            Command command = new Command("SP_GetOffersByType");
            command.AddParameter("Type", type);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Offer>());
        }

        public IEnumerable<Offer> GetByUser(int userId)
        {
            Command command = new Command("SP_GetByUser");
            command.AddParameter("Table", "Offers");
            command.AddParameter("UserId", userId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Offer>());
        }

        public IEnumerable<Offer> GetByUserAndCity(int userId, int cityId)
        {
            Command command = new Command("SP_GetOffersByUserAndCity");
            command.AddParameter("CreatorId", userId);
            command.AddParameter("CityId", cityId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Offer>());
        }

        public IEnumerable<Offer> GetByUserAndCompany(int userId, int companyId)
        {
            Command command = new Command("SP_GetOffersByUserAndCompany");
            command.AddParameter("CreatorId", userId);
            command.AddParameter("CompanyId", companyId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Offer>());
        }

        public IEnumerable<Offer> GetByUserAndContact(int userId, int contactId)
        {
            Command command = new Command("SP_GetOffersByUserAndCity");
            command.AddParameter("CreatorId", userId);
            command.AddParameter("ContactId", contactId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Offer>());
        }

        public IEnumerable<Offer> GetByUserAndType(int userId, string type)
        {
            Command command = new Command("SP_GetOffersByUserAndType");
            command.AddParameter("CreatorId", userId);
            command.AddParameter("Type", type);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Offer>());
        }

        public Offer Insert(Offer offer)
        {
            Command command = new Command("SP_AddOffer");
            command.AddParameter("Title", offer.Title);
            command.AddParameter("Description", offer.Description);
            command.AddParameter("PublicationDate", offer.PublicationDate);
            command.AddParameter("Type", offer.Type);
            command.AddParameter("Link", offer.Link);
            command.AddParameter("ContractType", offer.ContractType);
            command.AddParameter("UserId", offer.UserId);
            command.AddParameter("ContactId", offer.ContactId);
            command.AddParameter("CityId", offer.CityId);
            command.AddParameter("CompanyId", offer.CompanyId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<Offer>()).SingleOrDefault();
        }

        public bool Update(int id, Offer offer)
        {
            Command command = new Command("SP_UpdateOffer");
            command.AddParameter("Id", id);
            command.AddParameter("Title", offer.Title);
            command.AddParameter("Description", offer.Description);
            command.AddParameter("Type", offer.Type);
            command.AddParameter("Link", offer.Link);
            command.AddParameter("ContractType", offer.ContractType);

            return _connection.ExecuteNonQuery(command) == 1;
        }
    }
}
