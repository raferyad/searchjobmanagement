﻿using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Api.Models.Global.Services.Mappers;
using SearchJobManagement.Models.Repositories;
using Tools.Connections;

namespace SearchJobManagement.Api.Models.Global.Repositories
{
    public class UserRepository : IUserRepository<User>
    {

        private readonly Connection _connection;

        public UserRepository(Connection connection)
        {
            _connection = connection;
        }

        public bool Delete(int id)
        {
            Command command = new Command("SP_DeleteUser");
            command.AddParameter("Id", id);
            return _connection.ExecuteNonQuery(command) == 1;
        }

        public IEnumerable<User> GetAll()
        {
            Command command = new Command("SP_GetAllUsers");
            return _connection.ExecuteReader(command, dr => dr.MapTo<User>());
        }

        public User GetById(int id)
        {
            Command command = new Command("SP_GetUserById");
            command.AddParameter("Id", id);

            return _connection.ExecuteReader(command, dr => dr.MapTo<User>()).SingleOrDefault();
        }

        public User Insert(User user)
        {
            Command command = new Command("SP_InsertUser");
            command.AddParameter("LastName", user.LastName);
            command.AddParameter("FirstName", user.FirstName);
            command.AddParameter("Phone", user.Phone);
            command.AddParameter("BirthDate", user.BirthDate);
            command.AddParameter("Role", user.Role);
            command.AddParameter("Email", user.Email);
            command.AddParameter("Login", user.Login);
            command.AddParameter("Password", user.Password);

            return _connection.ExecuteReader(command, dr => dr.MapTo<User>()).SingleOrDefault();
        }

        public bool Update(int id, User user)
        {
            Command command = new Command("SP_UpdateUser");
            command.AddParameter("Id", id);
            command.AddParameter("LastName", user.LastName);
            command.AddParameter("FirstName", user.FirstName);
            command.AddParameter("Phone", user.Phone);
            command.AddParameter("BirthDate", user.BirthDate);

            return _connection.ExecuteNonQuery(command) == 1;
        }
    }
}
