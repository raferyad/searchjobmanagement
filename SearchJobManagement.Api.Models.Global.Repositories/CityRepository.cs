﻿using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Api.Models.Global.Services.Mappers;
using SearchJobManagement.Models.Repositories;
using Tools.Connections;

namespace SearchJobManagement.Api.Models.Global.Repositories
{
    public class CityRepository : ICityRepository<City>
    {

        private readonly Connection _connection;

        public CityRepository(Connection connection)
        {
            _connection = connection;
        }

        public bool Delete(int id)
        {
            Command command = new Command("SP_DeleteCity");
            command.AddParameter("Id", id);
            return _connection.ExecuteNonQuery(command) == 1;
        }

        public IEnumerable<City> GetAll()
        {
            Command command = new Command("SP_GetAllCities");
            return _connection.ExecuteReader(command, dr => dr.MapTo<City>());
        }

        public City GetById(int id)
        {
            Command command = new Command("SP_GetCityById");
            command.AddParameter("Id", id);

            return _connection.ExecuteReader(command, dr => dr.MapTo<City>()).SingleOrDefault();
        }

        public IEnumerable<City> GetByCountry(int countryId)
        {
            Command command = new Command("SP_GetByCountry");
            command.AddParameter("Table", "Cities");
            command.AddParameter("CountryId", countryId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<City>());
        }

        public City Insert(City city)
        {
            Command command = new Command("SP_AddCity");
            command.AddParameter("Name", city.Name);
            command.AddParameter("CounrtyId", city.CounrtyId);

            return _connection.ExecuteReader(command, dr => dr.MapTo<City>()).SingleOrDefault();
        }

        public bool Update(int id, City city)
        {
            Command command = new Command("SP_UpdateCity");
            command.AddParameter("Id", id);
            command.AddParameter("Name", city.Name);
            command.AddParameter("CounrtyId", city.CounrtyId);

            return _connection.ExecuteNonQuery(command) == 1;
        }
    }
}
