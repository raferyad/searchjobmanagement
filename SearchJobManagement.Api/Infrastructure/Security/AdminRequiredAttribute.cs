﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using SearchJobManagement.Api.Models.Client.Entities;

namespace SearchJobManagement.Api.Infrastructure.Security
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AdminRequiredAttribute : TypeFilterAttribute
    {
        public AdminRequiredAttribute() : base(typeof(AuthRequiredFilter))
        {
        }

        private class AuthRequiredFilter : IAuthorizationFilter
        {
            public void OnAuthorization(AuthorizationFilterContext context)
            {
                ITokenRepository? tokenService = (ITokenRepository?)context.HttpContext.RequestServices.GetService(typeof(ITokenRepository));

                context.HttpContext.Request.Headers.TryGetValue("Authorization", out StringValues authorizations);
                string? token = authorizations.SingleOrDefault(authorization => authorization.StartsWith("Bearer "));

                if (token is null)
                {
                    context.Result = new UnauthorizedResult();
                    return;
                }

                TokenUser? user = tokenService?.ValidateToken(token);

                if (user is null || user.Role != UserRole.ADMIN)
                {
                    context.Result = new UnauthorizedResult();
                    return;
                }
            }
        }
    }
}
