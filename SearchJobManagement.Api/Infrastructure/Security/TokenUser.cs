﻿using SearchJobManagement.Api.Models.Client.Entities;

namespace SearchJobManagement.Api.Infrastructure.Security
{
    public class TokenUser
    {
        public int Id { get; internal set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public UserRole Role { get; set; }
    }
}
