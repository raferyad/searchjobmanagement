﻿using SearchJobManagement.Api.Models.Client.Entities;
using SearchJobManagement.Models.Repositories;
using System.ComponentModel.DataAnnotations;

namespace SearchJobManagement.Api.Infrastructure.Validation
{

    [AttributeUsage(AttributeTargets.Property)]
    public class EmailExistsAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            IAuthRepository<UserBO> authRepository = (IAuthRepository<UserBO>)validationContext.GetService(typeof(IAuthRepository<UserBO>));
            string email = value as string;

            if (!string.IsNullOrWhiteSpace(email))
            {
                if (authRepository.EmailExists(email))
                {
                    return new ValidationResult("Email already exists in database!!");
                }
            }
            else
            {
                return new ValidationResult($"Invalid value in the field : {validationContext.MemberName}");
            }

            return ValidationResult.Success;
        }
    }
}
