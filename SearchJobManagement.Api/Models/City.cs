﻿namespace SearchJobManagement.Api.Models
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CounrtyId { get; set; }

        public City(string name, int counrtyId)
        {
            Name = name;
            CounrtyId = counrtyId;
        }

        internal City(int id, string name, int counrtyId)
            : this(name, counrtyId)
        {
            Id = id;
        }
    }
}
