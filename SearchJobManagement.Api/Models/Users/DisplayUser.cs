﻿using SearchJobManagement.Api.Models.Client.Entities;

namespace SearchJobManagement.Api.Models.Users
{
    public class DisplayUser
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
