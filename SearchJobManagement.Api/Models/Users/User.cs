﻿using SearchJobManagement.Api.Models.Client.Entities;

namespace SearchJobManagement.Api.Models.Users
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public DateTime BirthDate { get; set; }
        public bool Active { get; set; }
        public UserRole Role { get; set; }
        public string Token { get; set; }
    }
}
