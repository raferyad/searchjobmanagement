﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace SearchJobManagement.Api.Models.Users
{
    public class UpdateUserForm
    {
		[HiddenInput]
		public int Id { get; set; }

		[Required]
		[StringLength(50, MinimumLength = 8)]
		public string Login { get; set; }

		[Required]
		[EmailAddress]
		public string Email { get; set; }

		[Required]
		[StringLength(50, MinimumLength = 2)]
		public string LastName { get; set; }

		[Required]
		[StringLength(50, MinimumLength = 2)]
		public string FirstName { get; set; }

		[DataType(DataType.Date)]
		public DateTime? BirthDate { get; set; }
	}
}
