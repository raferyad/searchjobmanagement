﻿using System.ComponentModel.DataAnnotations;

namespace SearchJobManagement.Api.Models.Addresses
{
    public class DisplayAddress
    {
        public int Id { get; set; }
        public string? Street { get; set; }
        public int Number { get; set; }
        public int ZipCode { get; set; }
        public string? CityName { get; set; }
        public string? CountryName { get; set; }
    }
}
