﻿namespace SearchJobManagement.Api.Models
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Website { get; set; }
        public int CreatorId { get; set; }

        public Company(string name, string website, int creatorId)
        {
            Name = name;
            Website = website;
            CreatorId = creatorId;
        }

        internal Company(int id, string name, string website, int creatorId)
            : this(name, website, creatorId)
        {
            Id = id;
        }
    }
}
