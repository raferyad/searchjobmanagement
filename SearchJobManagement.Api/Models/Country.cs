﻿namespace SearchJobManagement.Api.Models
{
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Country(string name)
        {
            Name = name;
        }

        internal Country(int id, string name)
            : this(name)
        {
            Id = id;
        }
    }
}
