﻿namespace SearchJobManagement.Api.Models
{
    public class Candidacy
    {
        public int Id { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string Type { get; set; }
        public int OfferId { get; set; }
        internal int UserId { get; set; }

        public Candidacy(DateTime applicationDate, string type, int offerId, int userId)
        {
            ApplicationDate = applicationDate;
            Type = type;
            OfferId = offerId;
            UserId = userId;
        }

        internal Candidacy(int id, DateTime applicationDate, string type, int offerId, int userId)
            : this(applicationDate, type, offerId, userId)
        {
            Id = id;
        }

    }
}
