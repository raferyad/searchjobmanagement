﻿namespace SearchJobManagement.Api.Models
{
    public class Address
    {
        public int Id { get; set; }
        public string Street { get; set; }
        public int Number { get; set; }
        public int ZipCode { get; set; }
        public int CityId { get; set; }
        public int CompanyId { get; set; }

        public Address(string street, int number, int zipCode, int cityId, int companyId)
        {
            Street = street;
            Number = number;
            ZipCode = zipCode;
            CityId = cityId;
            CompanyId = companyId;
        }

        internal Address(int id, string street, int number, int zipCode, int cityId, int companyId)
            : this(street, number, zipCode, cityId, companyId)
        {
            Id = id;
        }

    }
}
