﻿using Microsoft.AspNetCore.Mvc;
using SearchJobManagement.Api.Infrastructure.Security;
using SearchJobManagement.Api.Models;
using SearchJobManagement.Api.Models.Client.Entities;
using SearchJobManagement.Forms;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SearchJobManagement.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AuthRequired]
    public class CandidacyController : ControllerBase
    {
        private readonly ICandidacyRepository<CandidacyBO> _candidacyRepository;
        private readonly IMappersService _mapper;
        private int UserId
        {
            get { return (int) ControllerContext.RouteData.Values["userId"]; }
        }

        public CandidacyController(ICandidacyRepository<CandidacyBO> candidacyRepository, IMappersService mapper)
        {
            _candidacyRepository = candidacyRepository;
            _mapper = mapper;
        }

        // GET: api/<CandidacyController>
        [HttpGet]
        public IEnumerable<Candidacy> Get()
        {
            return _candidacyRepository.GetByCreator(UserId).Select(c => _mapper.Map<CandidacyBO, Candidacy>(c));
        }

        // GET api/<CandidacyController>/5
        [HttpGet("{id}")]
        public Candidacy Get(int id)
        {
            return _mapper.Map<CandidacyBO, Candidacy>(_candidacyRepository.GetById(id));
        }

        // POST api/<CandidacyController>
        [HttpPost]
        public IActionResult Post([FromBody] Candidacy form)
        {
            form.UserId = UserId;

            CandidacyBO candidacyBO = _candidacyRepository.Insert(_mapper.Map<Candidacy, CandidacyBO>(form));
            Candidacy candidacyInserted = _mapper.Map<CandidacyBO, Candidacy>(candidacyBO);

            if(candidacyInserted is not null)
                return Ok(candidacyInserted);

            return BadRequest();
        }

        // PUT api/<CandidacyController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Candidacy form)
        {
            if (id != form.Id || form.UserId != UserId)
            {
                ModelState.AddModelError("Id", "Invalid Id");
                return BadRequest(ModelState);
            }

            if (_candidacyRepository.Update(id, _mapper.Map<Candidacy, CandidacyBO>(form)))
            {
                return Ok();
            }

            return BadRequest();
        }

        // DELETE api/<CandidacyController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (_candidacyRepository.Delete(UserId, id))
            {
                return Ok();
            }

            return BadRequest();
        }
    }
}
