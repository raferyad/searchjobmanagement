﻿using Microsoft.AspNetCore.Mvc;
using SearchJobManagement.Api.Infrastructure.Security;
using SearchJobManagement.Api.Models;
using SearchJobManagement.Api.Models.Client.Entities;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SearchJobManagement.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AuthRequired]
    public class CommentController : ControllerBase
    {
        private readonly ICommentRepository<CommentBO> _commentRepository;
        private int UserId
        {
            get { return (int)ControllerContext.RouteData.Values["userId"]; }
        }

        public CommentController(ICommentRepository<CommentBO> commentRepository)
        {
            _commentRepository = commentRepository;
        }

        // GET: api/<CommentController>
        [HttpGet]
        public IEnumerable<Comment> Get()
        {
            return _commentRepository.GetByCreator(UserId).Select(c => new Comment(c.Id, c.Content, c.CreationDate, c.Star, c.CreatorId, c.CompanyId, c.ParentId));
        }

        // GET api/<CommentController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            CommentBO cbo = _commentRepository.GetById(id);
            Comment comment = new Comment(cbo.Id, cbo.Content, cbo.CreationDate, cbo.Star, cbo.CreatorId, cbo.CompanyId, cbo.ParentId);
            if(comment is not null)
                return Ok(comment);

            return BadRequest("Element doesn't exist!");
        }

        // POST api/<CommentController>
        [HttpPost]
        public IActionResult Post([FromBody] Comment form)
        {
            form.CreatorId = UserId;

            CommentBO cbo = new CommentBO(form.Content, form.CreationDate, form.Star, form.CreatorId, form.CompanyId, form.ParentId);
            CommentBO cboInserted = _commentRepository.Insert(cbo);
            Comment commentInserted = new Comment(cboInserted.Id, cboInserted.Content, cboInserted.CreationDate, cboInserted.Star, cboInserted.CreatorId, cboInserted.CompanyId, cboInserted.ParentId);

            if (commentInserted is not null)
                return Ok(commentInserted);

            return BadRequest("Element isn't updated!");
        }

        // PUT api/<CommentController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Comment form)
        {
            if (id != form.Id || form.CreatorId != UserId)
            {
                ModelState.AddModelError("Id", "Invalid Id");
                return BadRequest(ModelState);
            }

            if (_commentRepository.Update(id, new CommentBO(form.Content, form.CreationDate, form.Star, form.CreatorId, form.CompanyId, form.ParentId)))
            {
                return Ok($"The element id = {id} has been updated!");
            }

            return BadRequest("Element isn't updated!");
        }

        // DELETE api/<CommentController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {

            if (_commentRepository.Delete(UserId, id))
            {
                return Ok($"The element id = {id} has been deleted!");
            }

            return BadRequest("Element isn't deleted!");
        }
    }
}
