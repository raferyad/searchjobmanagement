﻿using Microsoft.AspNetCore.Mvc;
using SearchJobManagement.Api.Infrastructure.Security;
using SearchJobManagement.Api.Models;
using SearchJobManagement.Api.Models.Client.Entities;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SearchJobManagement.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AuthRequired]
    public class OfferController : ControllerBase
    {
        private readonly IOfferRepository<OfferBO> _offerRepository;
        private readonly IMappersService _mapper;
        private int UserId
        {
            get { return (int)ControllerContext.RouteData.Values["userId"]; }
        }

        public OfferController(IOfferRepository<OfferBO> offerRepository, IMappersService mapper)
        {
            _offerRepository = offerRepository;
            _mapper = mapper;
        }

        // GET: api/<OfferController>
        [HttpGet]
        public IEnumerable<Offer> Get()
        {
            return _offerRepository.GetAll().Select(c => _mapper.Map<OfferBO, Offer>(c));
        }

        // GET api/<OfferController>/5
        [HttpGet("{id}")]
        public Offer Get(int id)
        {
            return _mapper.Map<OfferBO, Offer>(_offerRepository.GetById(id));
        }

        // POST api/<OfferController>
        [HttpPost]
        public IActionResult Post([FromBody] Offer form)
        {
            form.UserId = UserId;
            OfferBO offerBO = _offerRepository.Insert(_mapper.Map<Offer, OfferBO>(form));
            Offer offerInserted = _mapper.Map<OfferBO, Offer>(offerBO);

            if (offerInserted is not null)
                return Ok(offerInserted);

            return BadRequest();
        }

        // PUT api/<OfferController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Offer form)
        {
            if (id != form.Id || form.UserId != UserId)
            {
                ModelState.AddModelError("Id", "Invalid Id");
                return BadRequest(ModelState);
            }

            if (_offerRepository.Update(id, _mapper.Map<Offer, OfferBO>(form)))
            {
                return Ok();
            }

            return BadRequest();
        }

        // DELETE api/<OfferController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {

            if (_offerRepository.Delete(UserId, id))
            {
                return Ok();
            }

            return BadRequest();
        }
    }
}
