﻿using Microsoft.AspNetCore.Mvc;
using SearchJobManagement.Api.Infrastructure.Security;
using SearchJobManagement.Api.Models.Client.Entities;
using SearchJobManagement.Forms;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SearchJobManagement.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthRepository<UserBO> _authService;
        private readonly ITokenRepository _tokenRepository;
        private readonly IMappersService _mappersService;

        public AuthController(IAuthRepository<UserBO> authService, ITokenRepository tokenRepository, IMappersService mappersService)
        {
            _authService = authService;
            _tokenRepository = tokenRepository;
            _mappersService = mappersService;
        }

        // GET api/<AuthController>/5
        [HttpPost("Login")]
        public IActionResult Login([FromBody] LoginForm form)
        {

            UserBO user = _authService.Login(form.Email, form.Password);

            if (user is null)
                return Unauthorized(new { Error = "Invalid Email or Password" });

            user.Token = _tokenRepository.GenerateToken(new TokenUser() { Id = user.Id,
                LastName = user.LastName,
                FirstName = user.FirstName,
                Email = user.Email,
                Role = user.Role
            });

            return Ok(user);
        }

        // POST api/<AuthController>
        [HttpPost("Register")]
        public IActionResult Register([FromBody] RegisterForm form)
        {
            if (form == null)
                return Unauthorized(new { Error = "Invalid form" });

            if(!ModelState.IsValid)
                return BadRequest();

            bool isRegistered = _authService.Register(new UserBO(form.FirstName, form.LastName, form.Login, form.Email, form.Password, form.Phone, form.BirthDate));
            
            return Ok(isRegistered);
        }
    }
}
