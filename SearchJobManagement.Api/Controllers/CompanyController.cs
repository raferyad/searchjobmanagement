﻿using Microsoft.AspNetCore.Mvc;
using SearchJobManagement.Api.Infrastructure.Security;
using SearchJobManagement.Api.Models;
using SearchJobManagement.Api.Models.Client.Entities;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SearchJobManagement.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AuthRequired]
    public class CompanyController : ControllerBase
    {
        private readonly ICompanyRepository<CompanyBO> _companyRepository;
        private int UserId
        {
            get { return (int)ControllerContext.RouteData.Values["userId"]; }
        }

        public CompanyController(ICompanyRepository<CompanyBO> companyRepository)
        {
            _companyRepository = companyRepository;
        }

        // GET: api/<CompanyController>
        [HttpGet]
        public IEnumerable<Company> Get()
        {
            return _companyRepository.GetAll().Select(c => new Company(c.Id, c.Name, c.Website, c.CreatorId));
        }

        // GET api/<CompanyController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            CompanyBO c = _companyRepository.GetById(id);
            if(c is not null)
                return Ok(new Company(c.Id, c.Name, c.Website, c.CreatorId));

            return BadRequest("Element doesn't exist!");
        }

        // POST api/<CompanyController>
        [HttpPost]
        public IActionResult Post([FromBody] Company form)
        {
            CompanyBO company = new CompanyBO(form.Name, form.Website, UserId);
            CompanyBO cbo = _companyRepository.Insert(company);
            Company companyInserted = new Company(cbo.Id, cbo.Name, cbo.Website, cbo.CreatorId);

            if (companyInserted is not null)
                return Ok(companyInserted);

            return BadRequest("Element isn't inserted!");
        }

        // PUT api/<CompanyController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Company form)
        {
            if (id != form.Id || form.CreatorId != UserId)
            {
                ModelState.AddModelError("Id", "Invalid Id!");
                return BadRequest(ModelState);
            }

            if (_companyRepository.Update(id, new CompanyBO(form.Name, form.Website, UserId)))
            {
                return Ok($"The element id : {id} has been updated!");
            }

            return BadRequest("Element isn't updated!");
        }

        // DELETE api/<CompanyController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {

            if (_companyRepository.Delete(UserId, id))
            {
                return Ok($"The element id : {id} has been deleted!");
            }

            return BadRequest("Element isn't deleted!");
        }
    }
}
