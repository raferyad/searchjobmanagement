﻿using Microsoft.AspNetCore.Mvc;
using SearchJobManagement.Api.Infrastructure.Security;
using SearchJobManagement.Api.Models;
using SearchJobManagement.Api.Models.Client.Entities;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SearchJobManagement.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AuthRequired]
    public class ContactController : ControllerBase
    {
        private readonly IContactRepository<ContactBO> _contactRepository;

        public ContactController(IContactRepository<ContactBO> contactRepository)
        {
            _contactRepository = contactRepository;
        }

        // GET: api/<ContactController>
        [HttpGet]
        public IEnumerable<Contact> Get()
        {
            return _contactRepository.GetAll().Select(c => new Contact(c.Id, c.FirstName, c.LastName, c.Email, c.Phone));
        }

        // GET api/<ContactController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            ContactBO c = _contactRepository.GetById(id);
            if(c is not null)
                return Ok(new Contact(c.Id, c.FirstName, c.LastName, c.Email, c.Phone));

            return BadRequest("Element doesn't exist!");
        }

        // GET api/<ContactController>/offer/5
        [HttpGet("offer/{offerId}")]
        public IActionResult GetByOffer(int offerId)
        {
            ContactBO c = _contactRepository.GetByOffer(offerId);
            if(c is not null)
                return Ok(new Contact(c.Id, c.FirstName, c.LastName, c.Email, c.Phone));

            return BadRequest("Element doesn't exist!");
        }

        // POST api/<ContactController>
        [HttpPost]
        public IActionResult Post([FromBody] Contact form)
        {
            ContactBO cbo = _contactRepository.Insert(new ContactBO(form.FirstName, form.LastName, form.Email, form.Phone));
            Contact contactInserted = new Contact(cbo.Id, cbo.FirstName, cbo.LastName, cbo.Email, cbo.Phone);

            if (contactInserted is not null)
                return Ok(contactInserted);

            return BadRequest("Element isn't inserted!");
        }

        // PUT api/<ContactController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Contact form)
        {
            if (id != form.Id)
            {
                ModelState.AddModelError("Id", "Invalid Id");
                return BadRequest(ModelState);
            }

            if (_contactRepository.Update(id, new ContactBO(form.FirstName, form.LastName, form.Email, form.Phone)))
            {
                return Ok($"The element id = {id} has been updated!");
            }

            return BadRequest("Element isn't updated!");
        }

        // DELETE api/<ContactController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {

            if (_contactRepository.Delete(id))
            {
                return Ok($"The element id = {id} has been deleted!");
            }

            return BadRequest("Element isn't deleted!");
        }
    }
}
