using SearchJobManagement.Api.Infrastructure.Security;
using SearchJobManagement.Api.Models.Global.Repositories;
using SearchJobManagement.Api.Models.Global.Entities;
using SearchJobManagement.Api.Models.Client.Entities;
using SearchJobManagement.Api.Models.Client.Mappers;
using SearchJobManagement.Api.Models.Client.Services;
using SearchJobManagement.Models.Repositories;
using System.Data.SqlClient;
using Tools.Connections;
using Tools.Mappers;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "TestApi" });
});

var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddSingleton(sp => new Connection(connectionString, SqlClientFactory.Instance));

builder.Services.AddSingleton<IAuthRepository<User>, AuthRepository>();
builder.Services.AddSingleton<IUserRepository<User>, UserRepository>();
builder.Services.AddSingleton<ICandidacyRepository<Candidacy>, CandidacyRepository>();
builder.Services.AddSingleton<ICommentRepository<Comment>, CommentRepository>();
builder.Services.AddSingleton<ICompanyRepository<Company>, CompanyRepository>();
builder.Services.AddSingleton<IOfferRepository<Offer>, OfferRepository>();

builder.Services.AddSingleton<IAuthRepository<UserBO>, AuthService>();
builder.Services.AddSingleton<IUserRepository<UserBO>, UserService>();
builder.Services.AddSingleton<ICandidacyRepository<CandidacyBO>, CandidacyService>();
builder.Services.AddSingleton<ICommentRepository<CommentBO>, CommentService>();
builder.Services.AddSingleton<ICompanyRepository<CompanyBO>, CompanyService>();
builder.Services.AddSingleton<IOfferRepository<OfferBO>, OfferService>();

builder.Services.AddSingleton<ITokenRepository, TokenService>();

builder.Services.AddSingleton<IMappersService, MapperClient>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseRouting();

app.UseAuthorization();

app.MapControllers();

app.Run();
