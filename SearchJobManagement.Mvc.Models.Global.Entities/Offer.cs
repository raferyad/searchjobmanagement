﻿namespace SearchJobManagement.Mvc.Models.Global.Entities
{
    public class Offer
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ContractType { get; set; }
        public DateTime PublicationDate { get; set; }
        public string Link { get; set; }
        public string Type { get; set; }
        public int CityId { get; set; }
        public int ContactId { get; set; }
        public int CompanyId { get; set; }
        internal int UserId { get; set; }
    }
}
