﻿namespace SearchJobManagement.Mvc.Models.Global.Entities
{
    public enum UserRole
    {
        ADMIN, USER_SIMPLE, RECRUITER
    }
}