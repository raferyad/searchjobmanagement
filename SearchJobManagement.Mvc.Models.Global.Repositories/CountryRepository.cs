﻿using SearchJobManagement.Models.Repositories;
using SearchJobManagement.Mvc.Models.Global.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace SearchJobManagement.Mvc.Models.Global.Repositories
{
    public class CountryRepository : ICountryRepository<Country>
    {
        private readonly HttpClient _httpClient;

        public CountryRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public IEnumerable<Country> GetAll()
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync("api/country").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Country[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public Country GetById(int id)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/country/{id}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Country>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public Country Insert(Country country)
        {
            using (_httpClient)
            {
                string json = JsonSerializer.Serialize(country);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage httpResponseMessage = _httpClient.PostAsync("api/country/", httpContent).Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                return JsonSerializer.Deserialize<Country>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public bool Update(int id, Country country)
        {
            using (_httpClient)
            {
                string json = JsonSerializer.Serialize(country);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage httpResponseMessage = _httpClient.PutAsync($"api/country/{id}", httpContent).Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    throw new Exception("Problem encountered when trying to update the country!");
                }
                return true;
            }
        }

        public bool Delete(int id)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.DeleteAsync($"api/country/{id}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    throw new Exception("Problem encountered when trying to delete the country!");
                }
                return true;
            }
        }

    }
}
