﻿using SearchJobManagement.Models.Repositories;
using SearchJobManagement.Mvc.Models.Global.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace SearchJobManagement.Mvc.Models.Global.Repositories
{
    public class AddressRepository : IAddressRepository<Address>
    {
        private readonly HttpClient _httpClient;

        public AddressRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public IEnumerable<Address> GetAll()
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync("api/address").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Address[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public Address GetById(int id)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/address/{id}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Address>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public Address Insert(Address address)
        {
            using (_httpClient)
            {
                string json = JsonSerializer.Serialize(address);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage httpResponseMessage = _httpClient.PostAsync("api/address/", httpContent).Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                return JsonSerializer.Deserialize<Address>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public bool Update(int id, Address address)
        {
            using (_httpClient)
            {
                string json = JsonSerializer.Serialize(address);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage httpResponseMessage = _httpClient.PutAsync($"api/address/{id}", httpContent).Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    throw new Exception("Problem encountered when trying to update the address!");
                }
                return true;
            }
        }

        public bool Delete(int id)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.DeleteAsync($"api/address/{id}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    throw new Exception("Problem encountered when trying to delete the address!");
                }
                return true;
            }
        }

        public bool Delete(int userId, int id)
        {
            return Delete(id);
        }

        public IEnumerable<Address> GetByCompany(int companyId)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/address/company/{companyId}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Address[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public IEnumerable<Address> GetByCity(int cityId)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/address/city/{cityId}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Address[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }
    }
}
