﻿using SearchJobManagement.Models.Repositories;
using SearchJobManagement.Mvc.Models.Global.Entities;
using System.Net.Http.Headers;
using System.Text.Json;

namespace SearchJobManagement.Mvc.Models.Global.Repositories
{
    public class OfferRepository : IOfferRepository<Offer>
    {
        private readonly HttpClient _httpClient;

        public OfferRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public IEnumerable<Offer> GetAll()
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync("api/offer").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Offer[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public Offer GetById(int id)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/offer/{id}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Offer>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public Offer Insert(Offer offer)
        {
            using (_httpClient)
            {
                string json = JsonSerializer.Serialize(offer);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage httpResponseMessage = _httpClient.PostAsync("api/offer/", httpContent).Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                return JsonSerializer.Deserialize<Offer>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public bool Update(int id, Offer offer)
        {
            using (_httpClient)
            {
                string json = JsonSerializer.Serialize(offer);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage httpResponseMessage = _httpClient.PutAsync($"api/offer/{id}", httpContent).Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    throw new Exception("Problem encountered when trying to update the offer!");
                }
                return true;
            }
        }

        public bool Delete(int id)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.DeleteAsync($"api/offer/{id}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    throw new Exception("Problem encountered when trying to delete the offer!");
                }
                return true;
            }
        }

        public bool Delete(int userId, int id)
        {
            return Delete(id);
        }

        public bool Update(int userId, int id, Offer offer)
        {
            return Update(id, offer);
        }

        public IEnumerable<Offer> GetByType(string type)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/offer/type/{type}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Offer[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public IEnumerable<Offer> GetByCompany(int companyId)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/offer/company/{companyId}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Offer[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public IEnumerable<Offer> GetByCity(int cityId)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/offer/city/{cityId}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Offer[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public IEnumerable<Offer> GetByUserAndCompany(int userId, int companyId)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/offer/user/{userId}/company/{companyId}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Offer[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public IEnumerable<Offer> GetByUserAndCity(int userId, int cityId)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/offer/user/{userId}/city/{cityId}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Offer[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public IEnumerable<Offer> GetByUser(int userId)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/offer/user/{userId}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Offer[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public IEnumerable<Offer> GetByTitle(string title)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/offer/title/{title}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Offer[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public IEnumerable<Offer> GetByContact(int contactId)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/offer/contact/{contactId}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Offer[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public IEnumerable<Offer> GetByTitleAndCity(string title, string city)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/offer/title/{title}/city/{city}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Offer[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public IEnumerable<Offer> GetByUserAndType(int userId, string type)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/offer/user/{userId}/type/{type}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Offer[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public IEnumerable<Offer> GetByUserAndContact(int userId, int contactId)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/offer/user/{userId}/contact/{contactId}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Offer[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public bool ChangeType(int userId, int id, string type)
        {
            using (_httpClient)
            {
                string json = JsonSerializer.Serialize(type);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage httpResponseMessage = _httpClient.PutAsync($"api/offer/{id}/type", httpContent).Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    throw new Exception("Problem encountered when trying to update the offer status!");
                }
                return true;
            }
        }
    }
}
