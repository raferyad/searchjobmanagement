﻿using SearchJobManagement.Models.Repositories;
using SearchJobManagement.Mvc.Models.Global.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace SearchJobManagement.Mvc.Models.Global.Repositories
{
    public class ContactRepository : IContactRepository<Contact>
    {
        private readonly HttpClient _httpClient;

        public ContactRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public IEnumerable<Contact> GetAll()
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync("api/contact").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Contact[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public Contact GetById(int id)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/contact/{id}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Contact>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public Contact GetByOffer(int offerId)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/contact/offer/{offerId}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Contact>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public Contact Insert(Contact contact)
        {
            using (_httpClient)
            {
                string json = JsonSerializer.Serialize(contact);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage httpResponseMessage = _httpClient.PostAsync("api/contact/", httpContent).Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                return JsonSerializer.Deserialize<Contact>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public bool Update(int id, Contact contact)
        {
            using (_httpClient)
            {
                string json = JsonSerializer.Serialize(contact);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage httpResponseMessage = _httpClient.PutAsync($"api/contact/{id}", httpContent).Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    throw new Exception("Problem encountered when trying to update the contact!");
                }
                return true;
            }
        }

        public bool Delete(int id)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.DeleteAsync($"api/contact/{id}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    throw new Exception("Problem encountered when trying to delete the contact!");
                }
                return true;
            }
        }

        public bool Delete(int userId, int id)
        {
            return Delete(id);
        }
    }
}
