﻿using SearchJobManagement.Models.Repositories;
using SearchJobManagement.Mvc.Models.Global.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace SearchJobManagement.Mvc.Models.Global.Repositories
{
    public class CommentRepository : ICommentRepository<Comment>
    {
        private readonly HttpClient _httpClient;

        public CommentRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public IEnumerable<Comment> GetAll()
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync("api/comment").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Comment[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public Comment GetById(int id)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/comment/{id}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Comment>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public Comment Insert(Comment comment)
        {
            using (_httpClient)
            {
                string json = JsonSerializer.Serialize(comment);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage httpResponseMessage = _httpClient.PostAsync("api/comment/", httpContent).Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                return JsonSerializer.Deserialize<Comment>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public bool Update(int id, Comment comment)
        {
            using (_httpClient)
            {
                string json = JsonSerializer.Serialize(comment);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage httpResponseMessage = _httpClient.PutAsync($"api/comment/{id}", httpContent).Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    throw new Exception("Problem encountered when trying to update the comment!");
                }
                return true;
            }
        }

        public bool Delete(int id)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.DeleteAsync($"api/comment/{id}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();

                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    throw new Exception("Problem encountered when trying to delete the comment!");
                }
                return true;
            }
        }

        public IEnumerable<Comment> GetByCreator(int creatorId)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/comment/creator/{creatorId}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Comment[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public IEnumerable<Comment> GetByCompany(int companyId)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/comment/company/{companyId}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Comment[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public IEnumerable<Comment> GetByOffer(int offerId)
        {
            using (_httpClient)
            {
                HttpResponseMessage httpResponseMessage = _httpClient.GetAsync($"api/comment/offer/{offerId}").Result;
                httpResponseMessage.EnsureSuccessStatusCode();
                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;

                return JsonSerializer.Deserialize<Comment[]>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public bool Update(int userId, int id, Comment comment)
        {
            return Update(id, comment);
        }

        public bool Delete(int userId, int id)
        {
            return Delete(id);
        }
    }
}
