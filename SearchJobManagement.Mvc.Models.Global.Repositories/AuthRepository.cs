﻿using SearchJobManagement.Forms;
using SearchJobManagement.Models.Repositories;
using SearchJobManagement.Mvc.Models.Global.Entities;
using System.Net.Http.Headers;
using System.Text.Json;

namespace SearchJobManagement.Mvc.Models.Global.Repositories
{
    public class AuthRepository : IAuthRepository<User>
    {
        private readonly HttpClient _client;

        public AuthRepository(HttpClient client)
        {
            _client = client;
        }

        public bool EmailExists(string email)
        {
            throw new NotImplementedException();
        }

        public User Login(string email, string password)
        {
            using (_client)
            {
                string contentJson = JsonSerializer.Serialize(new { email, password });
                HttpContent httpContent = new StringContent(contentJson);
                httpContent.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");

                HttpResponseMessage httpResponseMessage = _client.PostAsync("api/auth/login", httpContent).Result;
                //httpResponseMessage.EnsureSuccessStatusCode();
                if (!httpResponseMessage.IsSuccessStatusCode)
                    return null;

                string json = httpResponseMessage.Content.ReadAsStringAsync().Result;
                return JsonSerializer.Deserialize<User>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
        }

        public bool Register(User entity)
        {
            using (_client)
            {
                RegisterForm form = new RegisterForm() {
                    LastName = entity.LastName,
                    FirstName = entity.FirstName,
                    Email = entity.Email,
                    Password = entity.Password,
                    Login = entity.Login,
                    BirthDate = entity.BirthDate,
                    Phone = entity.Phone
                };

                string json = JsonSerializer.Serialize(form);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage httpResponseMessage = _client.PostAsync("api/auth/register", httpContent).Result;
                //httpResponseMessage.EnsureSuccessStatusCode();

                return httpResponseMessage.IsSuccessStatusCode;
            }
        }
    }
}
