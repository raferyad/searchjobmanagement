﻿using SearchJobManagement.Mvc.Models.Client.Entities;
using G = SearchJobManagement.Mvc.Models.Global.Entities;
using Tools.Mappers;

namespace SearchJobManagement.Mvc.Models.Client.Mappers
{
    public class MapperClient : MappersService
    {
        protected override void ConfigureMappers(IMappersService service)
        {
            service.Register<UserBO, G.User>(u => new G.User()
            {
                Id = u.Id,
                FirstName = u.FirstName,
                LastName = u.LastName,
                Phone = u.Phone,
                Email = u.Email,
                Password = u.Password,
                Login = u.Login,
                BirthDate = u.BirthDate,
                Role = u.Role, // == UserRole.ADMIN ? "ADMIN" : "USER_SIMPLE"
                Token = u.Token
            });

            service.Register<G.User, UserBO>(u => new UserBO(
                u.Id, 
                u.FirstName, 
                u.LastName, 
                u.Login, 
                u.Email, 
                u.Password, 
                u.Phone, 
                u.BirthDate,
                u.Role, //== "ADMIN" ? UserRole.ADMIN : UserRole.USER_SIMPLE
                u.Token
                ));
        }
    }
}
