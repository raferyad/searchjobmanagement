﻿using SearchJobManagement.Mvc.Models.Client.Entities;
using SearchJobManagement.Mvc.Models.Global.Entities;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

namespace SearchJobManagement.Mvc.Models.Client.Services
{
    public class CompanyService : ICompanyRepository<CompanyBO>
    {

        private readonly ICompanyRepository<Company> _companyRepository;
        private readonly IMappersService _mappersService;

        public CompanyService(ICompanyRepository<Company> companyRepository, IMappersService mappersService)
        {
            _companyRepository = companyRepository;
            _mappersService = mappersService;
        }

        public bool Delete(int id)
        {
            return _companyRepository.Delete(id);
        }

        public bool Delete(int userId, int id)
        {
            return _companyRepository.Delete(userId, id);
        }

        public IEnumerable<CompanyBO> GetAll()
        {
            return _companyRepository.GetAll().Select(u => _mappersService.Map<Company, CompanyBO>(u));
        }

        public IEnumerable<CompanyBO> GetByCreator(int creatorId)
        {
            return _companyRepository.GetByCreator(creatorId).Select(c => _mappersService.Map<Company, CompanyBO>(c));
        }

        public CompanyBO GetById(int id)
        {
            return _mappersService.Map<Company, CompanyBO>(_companyRepository.GetById(id));
        }

        public IEnumerable<CompanyBO> GetByName(string name)
        {
            return _companyRepository.GetByName(name).Select(c => _mappersService.Map<Company, CompanyBO>(c));
        }

        public CompanyBO Insert(CompanyBO company)
        {
            CompanyBO companyBO = _mappersService.Map<Company, CompanyBO>(_companyRepository.Insert(_mappersService.Map<CompanyBO, Company>(company)));
            return companyBO;
        }

        public bool Update(int id, CompanyBO company)
        {
            return _companyRepository.Update(id, _mappersService.Map<CompanyBO, Company>(company));
        }

        public bool Update(int userId, int id, CompanyBO company)
        {
            return _companyRepository.Update(userId, id, _mappersService.Map<CompanyBO, Company>(company));
        }
    }
}
