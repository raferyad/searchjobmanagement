﻿using SearchJobManagement.Mvc.Models.Client.Entities;
using SearchJobManagement.Mvc.Models.Global.Entities;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

namespace SearchJobManagement.Mvc.Models.Client.Services
{
    public class ContactService : IContactRepository<ContactBO>
    {

        private readonly IContactRepository<Contact> _contactRepository;
        private readonly IMappersService _mappersService;

        public ContactService(IContactRepository<Contact> contactRepository, IMappersService mappersService)
        {
            _contactRepository = contactRepository;
            _mappersService = mappersService;
        }

        public bool Delete(int id)
        {
            return _contactRepository.Delete(id);
        }

        public bool Delete(int userId, int id)
        {
            return _contactRepository.Delete(userId, id);
        }

        public IEnumerable<ContactBO> GetAll()
        {
            return _contactRepository.GetAll().Select(u => _mappersService.Map<Contact, ContactBO>(u));
        }

        public ContactBO GetById(int id)
        {
            return _mappersService.Map<Contact, ContactBO>(_contactRepository.GetById(id));
        }

        public ContactBO GetByOffer(int offerId)
        {
            return _mappersService.Map<Contact, ContactBO>(_contactRepository.GetByOffer(offerId));
        }

        public ContactBO Insert(ContactBO contact)
        {
            return _mappersService.Map<Contact, ContactBO>(_contactRepository.Insert(_mappersService.Map<ContactBO, Contact>(contact)));
        }

        public bool Update(int id, ContactBO contact)
        {
            return _contactRepository.Update(id, _mappersService.Map<ContactBO, Contact>(contact));
        }
    }
}
