﻿using SearchJobManagement.Models.Repositories;
using SearchJobManagement.Mvc.Models.Client.Entities;
using Tools.Mappers;
using G = SearchJobManagement.Mvc.Models.Global.Entities;

namespace SearchJobManagement.Mvc.Models.Client.Services
{
    public class AuthService : IAuthRepository<UserBO>
    {
        public readonly IAuthRepository<G.User> _authRepository;
        public readonly IMappersService _mappersService;

        public AuthService(IAuthRepository<G.User> globalRepository, IMappersService mappersService)
        {
            _authRepository = globalRepository;
            _mappersService = mappersService;
        }

        public bool EmailExists(string email)
        {
            throw new NotImplementedException();
        }

        public UserBO Login(string email, string passwd)
        {
            return _mappersService.Map<G.User, UserBO>(_authRepository.Login(email, passwd));
        }

        public bool Register(UserBO user)
        {
            return _authRepository.Register(_mappersService.Map<UserBO, G.User>(user));
        }
    }
}
