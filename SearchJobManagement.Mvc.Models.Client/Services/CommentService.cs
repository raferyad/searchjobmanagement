﻿using SearchJobManagement.Mvc.Models.Client.Entities;
using SearchJobManagement.Mvc.Models.Global.Entities;
using SearchJobManagement.Models.Repositories;
using Tools.Mappers;

namespace SearchJobManagement.Mvc.Models.Client.Services
{
    public class CommentService : ICommentRepository<CommentBO>
    {

        private readonly ICommentRepository<Comment> _commentRepository;
        private readonly IMappersService _mappersService;

        public CommentService(ICommentRepository<Comment> commentRepository, IMappersService mappersService)
        {
            _commentRepository = commentRepository;
            _mappersService = mappersService;
        }

        public bool Delete(int id)
        {
            return _commentRepository.Delete(id);
        }

        public bool Delete(int userId, int id)
        {
            return _commentRepository.Delete(userId, id);
        }

        public IEnumerable<CommentBO> GetAll()
        {
            return _commentRepository.GetAll().Select(u => _mappersService.Map<Comment, CommentBO>(u));
        }

        public IEnumerable<CommentBO> GetByCompany(int companyId)
        {
            return _commentRepository.GetByCompany(companyId).Select(c => _mappersService.Map<Comment, CommentBO>(c));
        }

        public IEnumerable<CommentBO> GetByCreator(int creatorId)
        {
            return _commentRepository.GetByCreator(creatorId).Select(c => _mappersService.Map<Comment, CommentBO>(c));
        }

        public CommentBO GetById(int id)
        {
            return _mappersService.Map<Comment, CommentBO>(_commentRepository.GetById(id));
        }

        public IEnumerable<CommentBO> GetByOffer(int offerId)
        {
            return _commentRepository.GetByOffer(offerId).Select(c => _mappersService.Map<Comment, CommentBO>(c));
        }

        public CommentBO Insert(CommentBO comment)
        {
            return _mappersService.Map<Comment, CommentBO>(_commentRepository.Insert(_mappersService.Map<CommentBO, Comment>(comment)));
        }

        public bool Update(int id, CommentBO comment)
        {
            return _commentRepository.Update(id, _mappersService.Map<CommentBO, Comment>(comment));
        }

        public bool Update(int userId, int id, CommentBO comment)
        {
            return _commentRepository.Update(userId, id, _mappersService.Map<CommentBO, Comment>(comment));
        }
    }
}
