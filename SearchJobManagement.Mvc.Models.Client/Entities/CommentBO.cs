﻿namespace SearchJobManagement.Mvc.Models.Client.Entities
{
    public class CommentBO
    {
        public int Id { get; set; }
		public string Content { get; set; }
		public DateTime CreationDate { get; set; }
		public int Star { get; set; }
		public int CreatorId { get; set; }
		public int CompanyId { get; set; }
		public int ParentId { get; set; }

        public CommentBO(string content, DateTime creationDate, int star, int creatorId, int companyId, int parentId)
        {
            Content = content;
            CreationDate = creationDate;
            Star = star;
            CreatorId = creatorId;
            CompanyId = companyId;
            ParentId = parentId;
        }

        internal CommentBO(int id, string content, DateTime creationDate, int star, int creatorId, int companyId, int parentId)
            : this(content, creationDate, star, creatorId, companyId, parentId)
        {
            Id = id;
        }
    }
}
