﻿namespace SearchJobManagement.Mvc.Models.Client.Entities
{
    public class ContactBO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public ContactBO(string firstName, string lastName, string email, string phone)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Phone = phone;
        }

        internal ContactBO(int id, string firstName, string lastName, string email, string phone)
            : this(firstName, lastName, email, phone)
        {
            Id = id;
        }
    }
}
