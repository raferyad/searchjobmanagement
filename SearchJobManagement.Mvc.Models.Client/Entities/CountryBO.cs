﻿namespace SearchJobManagement.Mvc.Models.Client.Entities
{
    public class CountryBO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public CountryBO(string name)
        {
            Name = name;
        }

        internal CountryBO(int id, string name)
            : this(name)
        {
            Id = id;
        }
    }
}
