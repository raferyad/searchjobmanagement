﻿using System.ComponentModel.DataAnnotations;

namespace SearchJobManagement.Forms
{
    public class RegisterForm
    {
        [Required(ErrorMessage = "Le nom est obligatoire!")]
        [MaxLength(75)]
        [Display(Name = "Nom")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Le prénom est obligatoire!")]
        [MaxLength(75)]
        [Display(Name = "Prénom")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "L'email est obligatoire!")]
        [MaxLength(384)]
        [EmailAddress]
        public string Email { get; set; }
        public string Login { get; set; }
        [Display(Name = "Téléphone")]
        public string Phone { get; set; }
        
        [Display(Name = "Date de naissance")]
        [DataType(DataType.Date, ErrorMessage = "La date est invalide!")]
        public DateTime BirthDate { get; set; }
        
        [Required(ErrorMessage = "Le mot de passe est obligatoire!")]
        [MaxLength(20)]
        [RegularExpression(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-=]).{8,20}$")]
        [Display(Name = "Mot de passe")]
        public string Password { get; set; }
    }
}