﻿using System.ComponentModel.DataAnnotations;

namespace SearchJobManagement.Forms
{
    public class LoginForm
    {
        [Required(ErrorMessage = "L'email est obligatoire!")]
        [MaxLength(384)]
        [EmailAddress]
        public string Email { get; set; }
        [Required(ErrorMessage = "Le mot de passe est obligatoire!")]
        [MaxLength(20)]
        [Display(Name = "Mot de passe")]
        public string Password { get; set; }
    }
}
