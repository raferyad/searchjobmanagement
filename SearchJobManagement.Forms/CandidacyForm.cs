﻿using System.ComponentModel.DataAnnotations;

namespace SearchJobManagement.Forms
{
    public class CandidacyForm
    {
        [Display(Name = "Date de candidature")]
        [DataType(DataType.Date, ErrorMessage = "La date est invalide!")]
        public DateTime ApplicationDate { get; set; }
        //[Required(ErrorMessage = "Le type est obligatoire!")]
        public string? Type { get; set; }
        //[Required(ErrorMessage = "L'offre est obligatoire!")]
        //[Display(Name = "Offre")]
        //public int OfferId { get; set; }
        //[Required(ErrorMessage = "L est obligatoire!")]
        //[MaxLength(75)]
        //[Display(Name = "Prénom")]
        //internal int UserId { get; private set; }
    }
}