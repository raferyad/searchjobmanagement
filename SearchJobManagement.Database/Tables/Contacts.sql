﻿CREATE TABLE [dbo].[Contacts]
(
	[Id] INT NOT NULL IDENTITY,
	[LastName] VARCHAR(50) NOT NULL,
	[FirstName] VARCHAR(50) NOT NULL,
	[Email] VARCHAR(50) NOT NULL,
	[Phone] VARCHAR(12) NOT NULL,
    CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [UQ_ContactEmail] UNIQUE([Email])
)
