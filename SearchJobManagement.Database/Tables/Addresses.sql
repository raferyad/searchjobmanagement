﻿CREATE TABLE [dbo].[Addresses]
(
	[Id] INT NOT NULL IDENTITY,
	[Street] VARCHAR(50) NOT NULL,
	[Number] INT NOT NULL,
	[ZipCode] INT NOT NULL,
	[CityId] INT NOT NULL,
	[CompanyId] INT NOT NULL,
    CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_Address_City]
		FOREIGN KEY ([CityId]) REFERENCES [Cities]([Id]),
	CONSTRAINT [FK_Address_Company]
		FOREIGN KEY ([CompanyId]) REFERENCES [Companies]([Id])
)
