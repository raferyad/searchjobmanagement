﻿CREATE TABLE [dbo].[Offers]
(
	[Id] INT NOT NULL IDENTITY,
	[Title] VARCHAR(50) NOT NULL,
	[Description] Text,
	[ContractType] VARCHAR(50) NOT NULL,
	[PublicationDate] DATE,
	[Link] VARCHAR(100),
	[Type] VARCHAR(50) NOT NULL,
	[CityId] INT NOT NULL,
	[ContactId] INT,
	[CompanyId] INT NOT NULL,
	[CreatorId] INT NOT NULL,
    CONSTRAINT [PK_Offer] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_Offer_City]
		FOREIGN KEY ([CityId]) REFERENCES [Cities]([Id]),
	CONSTRAINT [FK_Offer_Contact]
		FOREIGN KEY ([ContactId]) REFERENCES [Contacts]([Id]),
	CONSTRAINT [FK_Offer_Company]
		FOREIGN KEY ([CompanyId]) REFERENCES [Companies]([Id]),
	CONSTRAINT [FK_Offer_User]
		FOREIGN KEY ([CreatorId]) REFERENCES [Users]([Id])
)
