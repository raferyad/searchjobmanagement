﻿CREATE TABLE [dbo].[Companies]
(
	[Id] INT NOT NULL IDENTITY,
	[Name] VARCHAR(50) NOT NULL,
	[Website] VARCHAR(100),
	[CreatorId] INT NOT NULL,
    CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_Company_User]
		FOREIGN KEY ([CreatorId]) REFERENCES [dbo].[Users]([Id])
)
