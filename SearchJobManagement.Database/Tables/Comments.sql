﻿CREATE TABLE [dbo].[Comments]
(
	[Id] INT NOT NULL IDENTITY,
	[Content] TEXT NOT NULL,
	[CreationDate] DateTIME NOT NULL,
	[Star] INT Default 0,
	[CreatorId] INT NOT NULL,
	[CompanyId] INT,
	[OfferId] INT,
	[ParentId] INT NULL,
	CONSTRAINT [PK_Comment] PRIMARY KEY([Id]),
	CONSTRAINT [FK_Comment_Comment] 
		FOREIGN KEY ([ParentId]) REFERENCES [dbo].[Comments]([Id]),
	CONSTRAINT [FK_Comment_User]
		FOREIGN KEY ([CreatorId]) REFERENCES [dbo].[Users]([Id]),
	CONSTRAINT [FK_Comment_Company]
		FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Companies]([Id]),
	CONSTRAINT [FK_Comment_Offer]
		FOREIGN KEY ([OfferId]) REFERENCES [dbo].[Offers]([Id])
)