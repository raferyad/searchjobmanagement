﻿CREATE TABLE [dbo].[Candidacies]
(
	[Id] INT NOT NULL IDENTITY,
	[ApplicationDate] DATE NOT NULL,
	[Type] VARCHAR(50) NOT NULL,
	[Status] VARCHAR(50) DEFAULT 'CREATED',
	[OfferId] INT NOT NULL,
	[UserId] INT NOT NULL,
    CONSTRAINT [PK_Candidacy] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_Candidacy_Offer]
		FOREIGN KEY ([OfferId]) REFERENCES [Offers]([Id]),
	CONSTRAINT [FK_Candidacy_User]
		FOREIGN KEY ([UserId]) REFERENCES [Users]([Id])
)
