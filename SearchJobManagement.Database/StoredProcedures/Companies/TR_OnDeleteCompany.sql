﻿CREATE Trigger [dbo].[TR_OnDeleteCompany]
	On [dbo].[Companies]
Instead Of Delete
As
Begin
	Delete from [dbo].[Comments]
		where [CompanyId] in (select [Id] from deleted)
End