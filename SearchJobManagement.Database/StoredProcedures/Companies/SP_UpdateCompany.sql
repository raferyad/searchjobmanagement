﻿CREATE PROCEDURE [AppUserSchema].[SP_UpdateCompany]
	@Id int,
	@Name varchar(15),
	@Website varchar(100)
As
Begin
	UPDATE [dbo].[Companies] 
		SET [Name] = @Name, [Website] = @Website
	Where [Id] = @Id
End