﻿CREATE PROCEDURE [AppUserSchema].[SP_DeleteCompany]
	@Id int
AS
	BEGIN
		Delete From [dbo].[Companies]
			Where [Id] = @Id;
	END