﻿CREATE PROCEDURE [AppUserSchema].[SP_AddCompany]
	@Name varchar(50),
	@Website varchar(100),
	@CreatorId int
As
Begin
	Insert into [dbo].[Companies] ([Name], [Website], [CreatorId]) OUTPUT inserted.Id
	Values (@Name, @Website, @CreatorId);
End