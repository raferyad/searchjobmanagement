﻿CREATE PROCEDURE [AppUserSchema].[SP_AddAddress]
	@Street VARCHAR(100),
	@Number INT,
	@ZipCode INT,
	@CityId INT,
	@CompanyId INT
As
Begin
	Insert into [dbo].[Addresses] ([Street], [Number], [ZipCode], [CityId],[CompanyId]) OUTPUT inserted.Id
	Values (@Street, @Number, @ZipCode, @CityId, @CompanyId);
End