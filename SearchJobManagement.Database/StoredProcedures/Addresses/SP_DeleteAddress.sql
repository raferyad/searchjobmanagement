﻿CREATE PROCEDURE [AppUserSchema].[SP_DeleteAddress]
	@Id int
AS
	BEGIN
		Delete From [dbo].[Addresses]
			Where [Id] = @Id;
	END