﻿CREATE PROCEDURE [AppUserSchema].[SP_GetByAddress]
	@Table nvarchar(255),
	@AddressId INT
AS
	DECLARE @sql NVARCHAR(64)

	SET @sql = 'SELECT * FROM [dbo].' + QUOTENAME(@Table) + ' Where [CompanyId] = ' + @AddressId
 
	EXEC (@sql)
RETURN 0