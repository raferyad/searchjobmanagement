﻿CREATE PROCEDURE [AppUserSchema].[SP_UpdateAddress]
	@Id int,
	@Street VARCHAR(100),
	@Number INT,
	@ZipCode INT
As
Begin
	UPDATE [dbo].[Addresses] 
		SET [Street] = @Street, [Number] = @Number, [ZipCode] = @ZipCode
	Where [Id] = @Id
End