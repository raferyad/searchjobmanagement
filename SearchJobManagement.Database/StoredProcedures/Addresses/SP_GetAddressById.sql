﻿CREATE PROCEDURE [AppUserSchema].[SP_GetAddressById]
	@Id int
AS
BEGIN
	Select * from [dbo].[Addresses]
	Where [Id] = @Id
END