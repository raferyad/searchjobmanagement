﻿CREATE PROCEDURE [AppUserSchema].[SP_GetByCity]
	@Table nvarchar(255),
	@CityId varchar(255)
AS
	DECLARE @sql NVARCHAR(64)

	SET @sql = 'SELECT * FROM [dbo].' + QUOTENAME(@Table) + ' Where [CompanyId] = ' + @CityId
 
	EXEC (@sql)
RETURN 0