﻿CREATE PROCEDURE [AppUserSchema].[SP_DeleteCity]
	@Id int
AS
	BEGIN
		Delete From [dbo].[Cities]
			Where [Id] = @Id;
	END