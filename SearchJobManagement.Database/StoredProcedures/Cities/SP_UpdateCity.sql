﻿CREATE PROCEDURE [AppUserSchema].[SP_UpdateCity]
	@Id int,
	@Name nvarchar(15)
As
Begin
	UPDATE [dbo].[Cities] 
		SET [Name] = @Name
	Where [Id] = @Id
End