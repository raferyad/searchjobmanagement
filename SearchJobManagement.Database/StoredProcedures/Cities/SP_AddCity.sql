﻿CREATE PROCEDURE [AppUserSchema].[SP_AddCity]
	@Name nvarchar(50)
As
Begin
	Insert into [dbo].[Cities] ([Name]) OUTPUT inserted.Id
	Values (@Name);
End