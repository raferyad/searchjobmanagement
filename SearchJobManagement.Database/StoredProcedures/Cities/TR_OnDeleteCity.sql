﻿CREATE Trigger [dbo].[TR_OnDeleteCity]
	On [dbo].[Cities]
Instead Of Delete
As
Begin
	Delete from [dbo].[Addresses]
		where [CityId] in (select [Id] from deleted)
End