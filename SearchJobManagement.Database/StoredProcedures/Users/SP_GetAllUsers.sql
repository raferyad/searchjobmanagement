﻿CREATE PROCEDURE [AppUserSchema].[SP_GetAllUsers]
	As
Begin
	Select [LastName], [FirstName], [Email], [Login], [BirthDate], [Role]
	From [dbo].[Users]
End
