﻿CREATE PROCEDURE [AppUserSchema].[SP_UpdateUser]
	@Id int,
	@LastName varchar(50),
	@FirstName varchar(50),
	@Phone varchar(15),
	@BirthDate Date
AS
BEGIN
	UPDATE [dbo].[Users] 
		SET [LastName] = @LastName, [FirstName] = @FirstName, [Phone] = @Phone, [BirthDate] = @BirthDate
		Where [Id] = @Id
END