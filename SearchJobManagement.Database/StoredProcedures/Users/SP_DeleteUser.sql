﻿CREATE PROCEDURE [AppUserSchema].[SP_DeleteUser]
	@Id int
As
Begin
	Delete
	From [dbo].[Users]
	where [Id] = @Id;
End