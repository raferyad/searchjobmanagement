﻿CREATE PROCEDURE [AppUserSchema].[SP_GetUserById]
	@id int
As
Begin
	Select [LastName], [FirstName], [Email], [Login], [BirthDate], [Role]
	From [dbo].[Users]
	Where [Id] = @id
End