﻿Create Trigger [dbo].[TR_OnDeleteUser]
On [dbo].[Users]
Instead Of Delete
As
Begin
	Update [dbo].[Users] Set [Active] = 0
		where [Id] in (select [Id] from deleted)
End