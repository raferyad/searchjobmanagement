﻿CREATE PROCEDURE [AppUserSchema].[SP_GetByContact]
	@Table nvarchar(255),
	@ContactId varchar(255)
AS
	DECLARE @sql NVARCHAR(64)

	SET @sql = 'SELECT * FROM [dbo].' + QUOTENAME(@Table) + ' Where [UserId] = ' + @ContactId
 
	EXEC (@sql)
RETURN 0