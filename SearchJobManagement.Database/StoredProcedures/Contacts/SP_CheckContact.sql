﻿CREATE PROCEDURE [AppUserSchema].[SP_CheckContact]
	@LastName nvarchar(50),
	@FirstName nvarchar(50)
As
Begin
	Select [LastName], [FirstName], [Email], [Phone]
	From [dbo].[Contacts]
	Where ([LastName] = @LastName AND [FirstName] = @FirstName)
End
