﻿CREATE PROCEDURE [AppUserSchema].[SP_DeleteContact]
	@Id int
As
Begin
	Delete
	From [dbo].[Contacts]
	where [Id] = @Id;
End