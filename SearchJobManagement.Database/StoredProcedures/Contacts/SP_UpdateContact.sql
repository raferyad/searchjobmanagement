﻿CREATE PROCEDURE [AppUserSchema].[SP_UpdateContact]
	@Id int,
	@LastName varchar(50),
	@FirstName varchar(50)
AS
BEGIN
	UPDATE [dbo].[Contacts] 
		SET [LastName] = @LastName, [FirstName] = @FirstName
		Where [Id] = @Id
END