﻿CREATE PROCEDURE [AppUserSchema].[SP_GetAllContacts]
	As
Begin
	Select [LastName], [FirstName], [Email], [Phone]
	From [dbo].[Contacts]
End
