﻿CREATE PROCEDURE [AppUserSchema].[SP_GetContactById]
	@id int
As
Begin
	Select [LastName], [FirstName], [Email], [Phone]
	From [dbo].[Contacts]
	Where [Id] = @id
End