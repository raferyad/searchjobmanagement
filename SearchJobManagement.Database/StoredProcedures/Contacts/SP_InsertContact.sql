﻿CREATE PROCEDURE [AppUserSchema].[SP_InsertContact]
	@LastName nvarchar(50),
	@FirstName nvarchar(50),
	@Email nvarchar(255),
	@Phone nvarchar(12)

As
Begin
	Insert into [dbo].[Contacts] ([LastName], [FirstName], [Email], [Phone])
	Values (@LastName, @FirstName, @Email, @Phone);
	select SCOPE_IDENTITY();
End