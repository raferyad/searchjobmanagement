﻿CREATE PROCEDURE [AppUserSchema].[SP_RegisterUser]
	@LastName varchar(50),
	@FirstName varchar(50),
	@Email varchar(255),
	@Login varchar(255),
	@Password varchar(20),
	@BirthDate DATETIME,
	@Phone varchar(15),
	@Role varchar(25)

As
Begin
	Insert into [dbo].[Users] ([LastName], [FirstName], [Email], [Login], [EncodedPassword], [BirthDate], [Phone], [Role]) OUTPUT INSERTED.Id
	Values (@LastName, @FirstName, @Email, @Login, [dbo].[SF_HashPassword](@Password), @BirthDate, @Phone, @Role)
End