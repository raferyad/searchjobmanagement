﻿CREATE PROCEDURE [AppUserSchema].[SP_CheckUser]
	@Email nvarchar(255),
	@Login nvarchar(255),
	@Password nvarchar(20)
As
Begin
	Select [LastName], [FirstName], [Email], [Login], [BirthDate], [Role]
	From [dbo].[Users]
	Where ([Email] = @Email OR [Login] = @Login)
		And [EncodedPassword] = [dbo].[SF_HashPassword](@Password)
End
