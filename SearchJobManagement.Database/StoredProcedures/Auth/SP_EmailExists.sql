﻿CREATE PROCEDURE [AppUserSchema].[SP_EmailExists]
	@Email VARCHAR(255)
AS
	SELECT Count(*)
	FROM [dbo].[Users]
	WHERE [Email] = @Email;
RETURN 0
