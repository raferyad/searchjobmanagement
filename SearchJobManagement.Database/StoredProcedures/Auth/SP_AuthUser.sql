﻿CREATE PROCEDURE [AppUserSchema].[SP_AuthUser]
	@Email nvarchar(255),
	@Password nvarchar(20)
As
Begin
	Select [Id], [LastName], [FirstName], [Email], [BirthDate], [Role], [Phone], [Login]
	From [dbo].[Users]
	Where ([Email] = @Email)
		And [EncodedPassword] = [dbo].[SF_HashPassword](@Password)
End
