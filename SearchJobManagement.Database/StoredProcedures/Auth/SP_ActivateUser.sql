﻿CREATE PROCEDURE [AppUserSchema].[SP_ActivateUser]
	@Id int
AS
BEGIN
	UPDATE [dbo].[Users] 
		SET [Active] = 1
		Where [Id] = @Id
END