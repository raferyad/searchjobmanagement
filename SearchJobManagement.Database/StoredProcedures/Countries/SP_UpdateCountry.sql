﻿CREATE PROCEDURE [AppUserSchema].[SP_UpdateCountry]
	@Id int,
	@Name nvarchar(15)
As
Begin
	UPDATE [dbo].[Countries] 
		SET [Name] = @Name
	Where [Id] = @Id
End