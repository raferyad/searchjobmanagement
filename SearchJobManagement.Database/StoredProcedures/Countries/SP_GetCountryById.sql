﻿CREATE PROCEDURE [AppUserSchema].[SP_GetCountryById]
	@Id int
AS
BEGIN
	Select * from [dbo].[Countries]
	Where [Id] = @Id
END