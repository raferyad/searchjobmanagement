﻿CREATE Trigger [dbo].[TR_OnDeleteCountry]
	On [dbo].[Countries]
Instead Of Delete
As
Begin
	Delete from [dbo].[Cities]
		where [CountryId] in (select [Id] from deleted)
End