﻿CREATE PROCEDURE [AppUserSchema].[SP_AddCountry]
	@Name nvarchar(50)
As
Begin
	Insert into [dbo].[Countries] ([Name]) OUTPUT inserted.Id
	Values (@Name);
End