﻿CREATE PROCEDURE [AppUserSchema].[SP_GetByCountry]
	@Table nvarchar(255),
	@CountryId varchar(255)
AS
	DECLARE @sql NVARCHAR(64)

	SET @sql = 'SELECT * FROM [dbo].' + QUOTENAME(@Table) + ' Where [CompanyId] = ' + @CountryId
 
	EXEC (@sql)
RETURN 0