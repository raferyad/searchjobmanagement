﻿CREATE PROCEDURE [AppUserSchema].[SP_DeleteCountry]
	@Id int
AS
	BEGIN
		Delete From [dbo].[Countries]
			Where [Id] = @Id;
	END