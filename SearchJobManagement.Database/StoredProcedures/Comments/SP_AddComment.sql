﻿CREATE PROCEDURE [AppUserSchema].[SP_AddComment]
	@CreatorId int ,
	@Content varchar(1000),
	@CompanyId int,
	@Star int,
	@ParentId int
AS
	Insert into [dbo].[Comments] ([Content], [CreationDate], [CreatorId], [CompanyId], [Star], [ParentId]) OUTPUT inserted.Id
	Values (@Content, GETDATE(), @CreatorId, @CompanyId, @Star, @ParentId);
	
RETURN 0
