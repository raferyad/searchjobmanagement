﻿CREATE PROCEDURE [AppUserSchema].[SP_UpdateComment]
	@Id int,
	@Content varchar(1000),
	@Star int
As
Begin
	UPDATE [dbo].[Comments] 
		SET [Content] = @Content, [Star] = @Star
	Where [Id] = @Id
End