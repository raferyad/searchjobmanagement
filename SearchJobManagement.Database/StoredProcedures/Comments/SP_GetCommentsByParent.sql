﻿CREATE PROCEDURE [AppUserSchema].[SP_GetCommentsByParent]
	@ParentId int
AS
BEGIN
	Select * from [dbo].[Comments]
	Where [ParentId] = @ParentId
END