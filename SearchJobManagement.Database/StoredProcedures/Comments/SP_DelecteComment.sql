﻿CREATE PROCEDURE [AppUserSchema].[SP_DelecteComment]
	@CreatorId int,
	@CommentId int
AS
	Delete From [dbo].[Comments]
		Where [CreatorId] = @CreatorId AND ([Id] = @CommentId OR [ParentId] = @CommentId)
RETURN 0