﻿CREATE PROCEDURE [AppUserSchema].[SP_GetLatestReviews]
AS
BEGIN
	Select TOP 6 * from [dbo].[Comments]
	ORDER BY [CreationDate] DESC
END