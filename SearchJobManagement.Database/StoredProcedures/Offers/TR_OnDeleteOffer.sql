﻿CREATE Trigger [dbo].[TR_OnDeleteOffer]
	On [dbo].[Offers]
Instead Of Delete
As
Begin
	Delete from [dbo].[Contacts]
		where [Id] in (select [ContactId] from deleted)

	--Delete from [dbo].[Comments]
	--	where [OfferId] in (select [Id] from deleted)
End