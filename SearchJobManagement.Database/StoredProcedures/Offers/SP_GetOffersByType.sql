﻿CREATE PROCEDURE [AppUserSchema].[SP_GetOffersByType]
	@Type varchar(50)
As
Begin
	select * From [dbo].[Offers]
	Where [Type] = @Type
End