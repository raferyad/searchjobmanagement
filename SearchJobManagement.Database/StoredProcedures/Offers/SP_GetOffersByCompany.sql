﻿CREATE PROCEDURE [AppUserSchema].[SP_GetOffersByCompany]
	@CompanyId int
As
Begin
	select * From [dbo].[Offers]
	Where [CompanyId] = @CompanyId
End