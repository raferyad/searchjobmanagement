﻿CREATE PROCEDURE [AppUserSchema].[SP_GetOffersByCity]
	@CityId int
As
Begin
	select * From [dbo].[Offers]
	Where [CityId] = @CityId
End