﻿CREATE PROCEDURE [AppUserSchema].[SP_GetOffersByUserAndCompany]
	@CreatorId int,
	@CompanyId int
As
Begin
	select * From [dbo].[Offers]
	Where [CreatorId] = @CreatorId AND [CompanyId] = @CompanyId
End