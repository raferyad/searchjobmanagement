﻿CREATE PROCEDURE [AppUserSchema].[SP_AddOffer]
	@Title VARCHAR(50),
	@Description TEXT,
	@ContractType VARCHAR(50),
	@PublicationDate DATE,
	@Link VARCHAR(100),
	@Type VARCHAR(50),
	@ContactId INT,
	@CompanyId INT,
	@CityId INT,
	@CreatorId INT

As
Begin
	Insert into [dbo].[Offers] ([Title], [Description], [ContractType], [PublicationDate], [Link], [Type], [ContactId], [CompanyId], [CityId], [CreatorId]) OUTPUT inserted.Id 
	Values (@Title, @Description, @ContractType, @PublicationDate, @Link, @Type, @ContactId, @CompanyId, @CityId, @CreatorId);
End