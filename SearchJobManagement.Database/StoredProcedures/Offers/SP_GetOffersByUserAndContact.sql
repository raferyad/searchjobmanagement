﻿CREATE PROCEDURE [AppUserSchema].[SP_GetOffersByUserAndContact]
	@CreatorId int,
	@ContactId int
As
Begin
	select * From [dbo].[Offers]
	Where [CreatorId] = @CreatorId AND [ContactId] = @ContactId
End

