﻿CREATE PROCEDURE [AppUserSchema].[SP_GetOffersByUserAndType]
	@CreatorId int,
	@Type varchar(50)
As
Begin
	select * From [dbo].[Offers]
	Where [CreatorId] = @CreatorId AND [Type] = @Type
End