﻿CREATE PROCEDURE [AppUserSchema].[SP_GetOffersByUserAndCity]
	@CreatorId int,
	@CityId int
As
Begin
	select * From [dbo].[Offers]
	Where [CreatorId] = @CreatorId AND [CityId] = @CityId
End