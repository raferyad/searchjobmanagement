﻿CREATE PROCEDURE [AppUserSchema].[SP_UpdateOffer]
	@Id int,
	@Title VARCHAR(50),
	@Description TEXT,
	@ContractType VARCHAR(50),
	@Link VARCHAR(100),
	@Type VARCHAR(50)

AS
BEGIN
	UPDATE [dbo].[Offers] 
		SET [Title] = @Title, [Description] = @Description, [ContractType] = @ContractType, [Link] = @Link, [Type] = @Type
			Where [Id] = @Id;
		RETURN;
END

