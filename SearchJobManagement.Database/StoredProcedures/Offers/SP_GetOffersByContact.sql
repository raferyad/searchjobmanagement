﻿CREATE PROCEDURE [AppUserSchema].[SP_GetOffersByContact]
	@ContactId int
As
Begin
	select * From [dbo].[Offers]
	Where [ContactId] = @ContactId
End