﻿CREATE PROCEDURE [AppUserSchema].[SP_GetByOffer]
	@Table nvarchar(255),
	@OfferId varchar(255)
AS
	DECLARE @sql NVARCHAR(64)

	SET @sql = 'SELECT * FROM [dbo].' + QUOTENAME(@Table) + ' Where [ServiceId] = ' + @OfferId
 
	EXEC (@sql)
RETURN 0
