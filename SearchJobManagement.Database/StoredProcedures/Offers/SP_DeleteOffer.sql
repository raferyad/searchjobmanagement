﻿CREATE PROCEDURE [AppUserSchema].[SP_DeleteOffer]
	@OfferId int
AS
	Delete From [dbo].[Offers]
		Where [Id] = @OfferId
RETURN 0