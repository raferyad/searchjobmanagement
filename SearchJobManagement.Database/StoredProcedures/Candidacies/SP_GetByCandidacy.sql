﻿CREATE PROCEDURE [AppUserSchema].[SP_GetByCandidacy]
	@Table nvarchar(255),
	@CandidacyId varchar(255)
AS
	DECLARE @sql NVARCHAR(64)

	SET @sql = 'SELECT * FROM [dbo].' + QUOTENAME(@Table) + ' Where [ServiceId] = ' + @CandidacyId
 
	EXEC (@sql)
RETURN 0
