﻿CREATE PROCEDURE [AppUserSchema].[SP_AddCandidacy]
	@Type VARCHAR(50),
	@ApplicationDate DATE,
	@UserId INT,
	@OfferId INT

As
Begin
	Insert into [dbo].[Candidacies] ([Type], [ApplicationDate], [UserId], [OfferId]) OUTPUT inserted.Id 
	Values (@Type, @ApplicationDate, @UserId, @OfferId);
End