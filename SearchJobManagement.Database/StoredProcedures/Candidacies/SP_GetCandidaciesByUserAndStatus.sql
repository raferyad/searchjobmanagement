﻿CREATE PROCEDURE [AppUserSchema].[SP_GetCandidaciesByUserAndStatus]
	@UserId int,
	@Status varchar(50)
As
Begin
	select * From [dbo].[Candidacies]
	Where [UserId] = @UserId AND [Status] = @Status
End