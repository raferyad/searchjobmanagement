﻿CREATE PROCEDURE [AppUserSchema].[SP_ChangeCandidacyStatus]
	@Id INT,
	@Status VARCHAR(50)

AS
BEGIN
	UPDATE [dbo].[Candidacies] 
		SET [Status] = @Status
			Where [Id] = @Id;
		RETURN;
END

