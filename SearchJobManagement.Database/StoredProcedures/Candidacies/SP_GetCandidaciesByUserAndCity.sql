﻿CREATE PROCEDURE [AppUserSchema].[SP_GetCandidaciesByUserAndCity]
	@UserId int,
	@CityId int
As
Begin
	select * From [dbo].[Candidacies]
	Where [UserId] = @UserId 
		AND [OfferId] = ( select [Id] from [dbo].[Offers]
			where [CityId] = @CityId
		)
End