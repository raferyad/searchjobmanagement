﻿CREATE PROCEDURE [AppUserSchema].[SP_GetCandidaciesByStatus]
	@Status varchar(50)
As
Begin
	select * From [dbo].[Candidacies]
	Where [Status] = @Status
End