﻿CREATE PROCEDURE [AppUserSchema].[SP_DeleteCandidacy]
	@CandidacyId int
AS
	Delete From [dbo].[Candidacies]
		Where [Id] = @CandidacyId
RETURN 0