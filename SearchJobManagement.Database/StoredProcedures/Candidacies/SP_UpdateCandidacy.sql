﻿CREATE PROCEDURE [AppUserSchema].[SP_UpdateCandidacy]
	@Id INT,
	@Type VARCHAR(50)

AS
BEGIN
	UPDATE [dbo].[Candidacies] 
		SET [Type] = @Type
			Where [Id] = @Id;
		RETURN;
END

