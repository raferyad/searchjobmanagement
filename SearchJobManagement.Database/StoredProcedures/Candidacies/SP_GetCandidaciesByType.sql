﻿CREATE PROCEDURE [AppUserSchema].[SP_GetCandidaciesByType]
	@Type varchar(50)
As
Begin
	select * From [dbo].[Candidacies]
	Where [Type] = @Type
End