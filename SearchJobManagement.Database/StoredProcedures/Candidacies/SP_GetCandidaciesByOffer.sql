﻿CREATE PROCEDURE [AppUserSchema].[SP_GetCandidaciesByOffer]
	@OfferId int
As
Begin
	select * From [dbo].[Candidacies]
	Where [OfferId] = @OfferId
End