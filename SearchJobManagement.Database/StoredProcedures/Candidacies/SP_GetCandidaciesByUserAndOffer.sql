﻿CREATE PROCEDURE [AppUserSchema].[SP_GetCandidaciesByUserAndOffer]
	@UserId int,
	@OfferId int
As
Begin
	select * From [dbo].[Candidacies]
	Where [UserId] = @UserId AND [OfferId] = @OfferId
End