﻿CREATE PROCEDURE [AppUserSchema].[SP_GetCandidaciesByUserAndCompany]
	@UserId int,
	@CompanyId int
As
Begin
	select * From [dbo].[Candidacies]
	Where [UserId] = @UserId 
		AND [OfferId] = ( select [Id] from [dbo].[Offers]
			where [CompanyId] = @CompanyId
		)
End