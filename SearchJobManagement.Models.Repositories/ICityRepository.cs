﻿namespace SearchJobManagement.Models.Repositories
{
    public interface ICityRepository<TCity> : IRepository<TCity> where TCity : class
    {
        IEnumerable<TCity> GetByCountry(int countryId);
    }
}
