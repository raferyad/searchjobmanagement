﻿namespace SearchJobManagement.Models.Repositories
{
    public interface IAddressRepository<TAddress> : IRepository<TAddress> where TAddress : class
    {
        IEnumerable<TAddress> GetByCompany(int companyId);
        IEnumerable<TAddress> GetByCity(int cityId);
        bool Delete(int userId, int id);
    }
}
