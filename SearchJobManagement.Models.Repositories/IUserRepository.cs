﻿namespace SearchJobManagement.Models.Repositories
{
    public interface IUserRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
    }
}
