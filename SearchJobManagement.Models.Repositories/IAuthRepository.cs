﻿namespace SearchJobManagement.Models.Repositories
{
    public interface IAuthRepository<TUser> where TUser : class
    {
        TUser Login(string email, string password);
        bool Register(TUser user);
        bool EmailExists(string email);
    }
}
