﻿namespace SearchJobManagement.Models.Repositories
{
    public interface ICountryRepository<TCountry> : IRepository<TCountry> where TCountry : class
    {
    }
}
