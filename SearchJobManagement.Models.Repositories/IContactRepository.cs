﻿namespace SearchJobManagement.Models.Repositories
{
    public interface IContactRepository <TContact> : IRepository<TContact> where TContact : class
    {
        TContact GetByOffer(int offerId);
        bool Delete(int userId, int id);
    }
}
