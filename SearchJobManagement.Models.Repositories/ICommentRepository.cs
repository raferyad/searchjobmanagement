﻿namespace SearchJobManagement.Models.Repositories
{
    public interface ICommentRepository<TComment> : IRepository<TComment> where TComment : class
    {
        IEnumerable<TComment> GetByCreator(int creatorId);
        IEnumerable<TComment> GetByCompany(int companyId);
        IEnumerable<TComment> GetByOffer(int offerId);
        bool Update(int userId, int id, TComment comment);
        bool Delete(int userId, int id);
    }
}
