﻿namespace SearchJobManagement.Models.Repositories
{
    public interface ICandidacyRepository<TCandidacy> : IRepository<TCandidacy> where TCandidacy : class
    {
        IEnumerable<TCandidacy> GetByCreator(int creatorId);
        IEnumerable<TCandidacy> GetByStatus(string status);
        IEnumerable<TCandidacy> GetByType(string type);
        IEnumerable<TCandidacy> GetByOffer(int offerId);
        IEnumerable<TCandidacy> GetByCompany(int companyId);
        IEnumerable<TCandidacy> GetByCity(int cityId);
        IEnumerable<TCandidacy> GetByUserAndStatus(int userId, string status);
        IEnumerable<TCandidacy> GetByUserAndOffer(int userId, int offerId);
        IEnumerable<TCandidacy> GetByUserAndCompany(int userId, int companyId);
        IEnumerable<TCandidacy> GetByUserAndCity(int userId, int cityId);
        bool Delete(int userId, int id);
        bool ChangeStatus(int userId, int id, string status);
    }
}
