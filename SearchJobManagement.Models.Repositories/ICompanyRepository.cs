﻿namespace SearchJobManagement.Models.Repositories
{
    public interface ICompanyRepository<TCompany> : IRepository<TCompany> where TCompany : class
    {
        IEnumerable<TCompany> GetByCreator(int creatorId);
        IEnumerable<TCompany> GetByName(string name);
        bool Delete(int userId, int id);
        bool Update(int userId, int id, TCompany company);
    }
}
