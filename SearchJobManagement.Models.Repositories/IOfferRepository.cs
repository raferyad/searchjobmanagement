﻿namespace SearchJobManagement.Models.Repositories
{
    public interface IOfferRepository<TOffer> : IRepository<TOffer> where TOffer : class
    {
        IEnumerable<TOffer> GetByUser(int userId);
        IEnumerable<TOffer> GetByTitle(string title);
        IEnumerable<TOffer> GetByType(string type);
        IEnumerable<TOffer> GetByContact(int contactId);
        IEnumerable<TOffer> GetByCompany(int companyId);
        IEnumerable<TOffer> GetByCity(int cityId);
        IEnumerable<TOffer> GetByTitleAndCity(string title, string city);
        IEnumerable<TOffer> GetByUserAndType(int userId, string type);
        IEnumerable<TOffer> GetByUserAndContact(int userId, int contactId);
        IEnumerable<TOffer> GetByUserAndCompany(int userId, int companyId);
        IEnumerable<TOffer> GetByUserAndCity(int userId, int cityId);
        bool Delete(int userId, int id);
        bool ChangeType(int userId, int id, string type);
    }
}
